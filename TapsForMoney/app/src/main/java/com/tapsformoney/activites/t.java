package com.tapsformoney.activites;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Dell on 9/28/2016.
 */
public class t {
    private static final String TAG = "Chartboost";

    private AppLovinIncentivizedInterstitial incentivizedInterstitial;

    private static Chartboost cb;

    public void loadads(final Context context, final WeakReference<p> weakRef) {
        p moreActivity = (p) context;


        final AppLovinSdk sdk = AppLovinSdk.getInstance(context);

        incentivizedInterstitial = AppLovinIncentivizedInterstitial.create(context);
        incentivizedInterstitial.preload(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                showadds(weakRef, context);
            }


            @Override
            public void failedToReceiveAd(int errorCode) {
                Toast.makeText(context, "Rewarded video failed to load with error code " + errorCode, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showadds(WeakReference<p> weakRef, final Context context) {
        if (incentivizedInterstitial.isAdReadyToDisplay()) {

            //
            // OPTIONAL: Create listeners
            //

            // Reward Listener
            AppLovinAdRewardListener adRewardListener = new AppLovinAdRewardListener() {
                @Override
                public void userRewardVerified(AppLovinAd appLovinAd, Map map) {
                    // AppLovin servers validated the reward. Refresh user balance from your server.  We will also pass the number of coins
                    // awarded and the name of the currency.  However, ideally, you should verify this with your server before granting it.

                    // i.e. - "Coins", "Gold", whatever you set in the dashboard.
                    String currencyName = (String) map.get("currency");

                    // For example, "5" or "5.00" if you've specified an amount in the UI.
                    String amountGivenString = (String) map.get("amount");

                    log("Rewarded " + amountGivenString + " " + currencyName, context);
                    log("", context);
                    // By default we'll show a alert informing your user of the currency & amount earned.
                    // If you don't want this, you can turn it off in the Manage Apps UI.
                }

                @Override
                public void userOverQuota(AppLovinAd appLovinAd, Map map) {
                    // Your user has already earned the max amount you allowed for the day at this point, so
                    // don't give them any more money. By default we'll show them a alert explaining this,
                    // though you can change that from the AppLovin dashboard.
                }

                @Override
                public void userRewardRejected(AppLovinAd appLovinAd, Map map) {
                    // Your user couldn't be granted a reward for this view. This could happen if you've blacklisted
                    // them, for example. Don't grant them any currency. By default we'll show them an alert explaining this,
                    // though you can change that from the AppLovin dashboard.
                }

                @Override
                public void validationRequestFailed(AppLovinAd appLovinAd, int responseCode) {
                    if (responseCode == AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO) {
                        // Your user exited the video prematurely. It's up to you if you'd still like to grant
                        // a reward in this case. Most developers choose not to. Note that this case can occur
                        // after a reward was initially granted (since reward validation happens as soon as a
                        // video is launched).
                    } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT || responseCode == AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR) {
                        // Some server issue happened here. Don't grant a reward. By default we'll show the user
                        // a alert telling them to try again later, but you can change this in the
                        // AppLovin dashboard.
                    } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED) {
                        // Indicates that the developer called for a rewarded video before one was available.
                        // Note: This code is only possible when working with rewarded videos.
                    }
                }

                @Override
                public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                    // This method will be invoked if the user selected "no" when asked if they want to view an ad.
                    // If you've disabled the pre-video prompt in the "Manage Apps" UI on our website, then this method won't be called.
                }
            };

            // Video Playback Listener
            AppLovinAdVideoPlaybackListener adVideoPlaybackListener = new AppLovinAdVideoPlaybackListener() {
                @Override
                public void videoPlaybackBegan(AppLovinAd appLovinAd) {
                    log("Video Started", context);
                }

                @Override
                public void videoPlaybackEnded(AppLovinAd appLovinAd, double v, boolean b) {
                    log("Video Ended", context);
                    increaseOneTap(context);

                }
            };

            // Ad Dispaly Listener
            AppLovinAdDisplayListener adDisplayListener = new AppLovinAdDisplayListener() {
                @Override
                public void adDisplayed(AppLovinAd appLovinAd) {
                    log("Ad Displayed", context);
                }

                @Override
                public void adHidden(AppLovinAd appLovinAd) {
                    log("Ad Dismissed", context);
                }
            };

            // Ad Click Listener
            AppLovinAdClickListener adClickListener = new AppLovinAdClickListener() {
                @Override
                public void adClicked(AppLovinAd appLovinAd) {
                    log("Ad Click", context);
                }
            };

                    /*
                     NOTE: We recommend the use of placements (AFTER creating them in your dashboard):

                     incentivizedInterstitial.show("REWARDED_VIDEO_DEMO_SCREEN", adRewardListener, adVideoPlaybackListener, adDisplayListener, adClickListener);

                     To learn more about placements, check out https://applovin.com/integration#androidPlacementsIntegration
                     */
            incentivizedInterstitial.show(weakRef.get(), adRewardListener, adVideoPlaybackListener, adDisplayListener, adClickListener);
        }
    }

    private void log(String text, Context context) {
//            Toast.makeText(context,text,Toast.LENGTH_SHORT).show();

    }

    private void increaseOneTap(Context context) {
        int tapsLeft = SharedPrefs.getInt(context, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(context, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.SESSION_TAPS += 1;
        SharedPrefs.save(context, SharedPrefs.TOTAL_TAPS, tapsLeft -= 225);
        SharedPrefs.save(context, SharedPrefs.EREND_TAPS, tapsErned += 225);

    }

    Context context;

    public void chartboostaddshowing(Context context, Chartboost cb) {
        this.context = context;
        this.cb = cb;
//        this.cb = Chartboost.sharedChartboost();
//        String appId = "4f7b433509b6025804000002";
//        String appSignature = "dd2d41b69ac01b80f443f5b6cf06096d457f82bd";
//        this.cb.onCreate((p)context, appId, appSignature, this.chartBoostDelegate);
//
//        this.cb.onStart((p)context);
//
        this.cb.showInterstitial();
    }

    public static ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {


        @Override
        public boolean shouldDisplayInterstitial(String location) {
            return true;
        }


        @Override
        public boolean shouldRequestInterstitial(String location) {
            return true;
        }


        @Override
        public void didCacheInterstitial(String location) {
        }


        @Override
        public void didFailToLoadInterstitial(String location, CBError.CBImpressionError error) {

        }


        @Override
        public void didDismissInterstitial(String location) {

            cb.cacheInterstitial(location);


        }


        @Override
        public void didCloseInterstitial(String location) {

        }


        @Override
        public void didClickInterstitial(String location) {

        }


        @Override
        public void didShowInterstitial(String location) {
        }


        @Override
        public void didFailToRecordClick(String uri, CBError.CBClickError error) {

        }


        @Override
        public boolean shouldDisplayLoadingViewForMoreApps() {
            return true;
        }


        @Override
        public boolean shouldRequestMoreApps() {

            return true;
        }


        @Override
        public boolean shouldDisplayMoreApps() {
            return true;
        }


        @Override
        public void didFailToLoadMoreApps(CBError.CBImpressionError error) {

        }

        @Override
        public void didCacheMoreApps() {
        }


        @Override
        public void didDismissMoreApps() {

        }


        @Override
        public void didCloseMoreApps() {

        }

        @Override
        public void didClickMoreApps() {

        }


        @Override
        public void didShowMoreApps() {
        }


        @Override
        public boolean shouldRequestInterstitialsInFirstSession() {
            return true;
        }

        @Override
        public boolean shouldPauseClickForConfirmation(
                Chartboost.CBAgeGateConfirmation callback) {
            // TODO Auto-generated method stub
            return false;
        }
    };

}
