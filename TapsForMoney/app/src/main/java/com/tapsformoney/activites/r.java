package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chartboost.sdk.Chartboost;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.CheckNetworkConnection;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class r extends Activity implements View.OnTouchListener {

    boolean reachedLimt = false;
    LinearLayout coin_left, coin_right;
    TextView status_view, total_coins, back_view, goagain, quit;
    boolean firstTime = true;
    int correct = 1;
    int increm = 1;

    ImageView coin_left_image, coin_right_image;
    WeakReference<r> weakRef;
    Chartboost cb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b.index = 0;
        setContentView(R.layout.activity_play_game_of_odds);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        weakRef = new WeakReference<r>(this);
        getn();

        init();
        if (CheckNetworkConnection.isConnectionAvailable(this)) {

        } else {
            findViewById(R.id.table).setVisibility(View.INVISIBLE);
        }

        cb = Chartboost.sharedChartboost();
        String appId = getResources().getString(R.string.shartboost_appid);
        String appSignature = getResources().getString(R.string.shartboost_appsignature);
        cb.onCreate(this, appId, appSignature, t.chartBoostDelegate);

        cb.onStart(this);
    }

    private void init() {
        coin_left = (LinearLayout) findViewById(R.id.coin_left);
        coin_right = (LinearLayout) findViewById(R.id.coin_right);
        back_view = (TextView) findViewById(R.id.back_view);
        total_coins = (TextView) findViewById(R.id.total_coins);
        status_view = (TextView) findViewById(R.id.status_view);
        quit = (TextView) findViewById(R.id.quit);
        goagain = (TextView) findViewById(R.id.goagain);
        coin_left_image = (ImageView) findViewById(R.id.coin_left_image);
        coin_right_image = (ImageView) findViewById(R.id.coin_right_image);
        coin_left.setOnTouchListener(this);
        coin_right.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
        goagain.setOnTouchListener(this);
        quit.setOnTouchListener(this);
        updateTaps();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:

                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.goagain:

                findViewById(R.id.gameover).setVisibility(View.GONE);
                findViewById(R.id.table).setVisibility(View.VISIBLE);

                reset();

                break;
            case R.id.quit:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.coin_left:

                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    b.showAdss(r.this, null, weakRef, null, null, cb);
                    correct = generateRandom();
                    status_view.setVisibility(View.GONE);
                    coin_left.setVisibility(View.VISIBLE);
                    increaseOneTap(increm, 0);
                } else {
                    findViewById(R.id.table).setVisibility(View.INVISIBLE);
                }


                break;
            case R.id.coin_right:
                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    if (!firstTime) {
                        correct = generateRandom();

                    } else {
                        b.showAdss(r.this, true, null, weakRef, null, null, cb);

                    }
                    b.showAdss(r.this, null, weakRef, null, null, cb);

                    firstTime = false;
                    status_view.setVisibility(View.GONE);
                    coin_left.setVisibility(View.VISIBLE);
                    increaseOneTap(increm, 1);

                } else {
                    findViewById(R.id.table).setVisibility(View.INVISIBLE);
                }

                break;


        }
    }


    private void updateTaps() {
        total_coins.setText("Total Coins: " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_COINS, 0));

    }

    private void increaseOneTap(int incremnt, int answer) {

        if (!reachedLimt) {
            if (answer == correct) {
                int coins = SharedPrefs.getInt(this, SharedPrefs.TOTAL_COINS, 0);

                SharedPrefs.save(this, SharedPrefs.TOTAL_COINS, coins += increm);

                switch (incremnt) {


                    case 1:
                        increm = 2;
                        coin_right_image.setImageResource(R.drawable.coin2);
                        coin_left_image.setImageResource(R.drawable.coin2);
                        break;
                    case 2:
                        increm = 4;
                        coin_right_image.setImageResource(R.drawable.coin4);
                        coin_left_image.setImageResource(R.drawable.coin4);
                        break;
                    case 4:
                        increm = 10;
                        coin_right_image.setImageResource(R.drawable.coin10);
                        coin_left_image.setImageResource(R.drawable.coin10);
                        break;
                    case 10:
                        increm = 20;
                        coin_right_image.setImageResource(R.drawable.coin20);
                        coin_left_image.setImageResource(R.drawable.coin20);
                        break;
                    case 20:
                        increm = 100;
                        coin_right_image.setImageResource(R.drawable.coin100);
                        coin_left_image.setImageResource(R.drawable.coin100);
                        break;

                    case 100:
                        increm = 300;
                        coin_right_image.setImageResource(R.drawable.coin300);
                        coin_left_image.setImageResource(R.drawable.coin300);
                        break;
                    case 300:
                        increm = 1000;
                        coin_right_image.setImageResource(R.drawable.coin1000);
                        coin_left_image.setImageResource(R.drawable.coin1000);
                        break;
                    case 1000:
                        increm = 1500;
                        coin_right_image.setImageResource(R.drawable.coin1500);
                        coin_left_image.setImageResource(R.drawable.coin1500);
                        break;
                    case 1500:
                        increm = 10000;
                        coin_right_image.setImageResource(R.drawable.coin10000);
                        coin_left_image.setImageResource(R.drawable.coin10000);
                        break;
                    case 10000:
                        increm = 15000;
                        coin_right_image.setImageResource(R.drawable.coin15000);
                        coin_left_image.setImageResource(R.drawable.coin15000);
                        break;
                    case 15000:
                        increm = 25000;
                        coin_right_image.setImageResource(R.drawable.coin25000);
                        coin_left_image.setImageResource(R.drawable.coin25000);
                        break;
                    case 25000:
                        increm = 50000;
                        coin_right_image.setImageResource(R.drawable.coin50000);
                        coin_left_image.setImageResource(R.drawable.coin50000);
                        reachedLimt = true;
                        break;
                }


                updateTaps();
            } else {
                status_view.setVisibility(View.VISIBLE);

                status_view.setText("So Close! Try Again!");
                findViewById(R.id.gameover).setVisibility(View.VISIBLE);
                findViewById(R.id.table).setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.table).setVisibility(View.INVISIBLE);
        }

    }

    private void reset() {
        correct = 1;
        firstTime = true;
        coin_left_image.setImageResource(R.drawable.coin1);
        coin_right_image.setImageResource(R.drawable.coin1);
        status_view.setText("Get +1 coin to start");
        status_view.setVisibility(View.VISIBLE);
        coin_left.setVisibility(View.GONE);
        increm = 1;
    }

    private int generateRandom() {
        Random rn = new Random();
        return rn.nextInt(1 - 0 + 1) + 0;
    }

    private int generateAddsRandom() {
        Random rn = new Random();
        return rn.nextInt(1 - 0 + 1) + 0;
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}

