package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.CheckNetworkConnection;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class f extends Activity implements View.OnTouchListener {


    ImageView play_button_view;
    LinearLayout how_to_play_view, ticket_prize_view, buy_ticketq_view, submit_odds_tickets_view;
    TextView back_view, total_coin, total_tickets;
    int coins, tickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gemeof_odds_activty);
        getn();
        coins = SharedPrefs.getInt(this, SharedPrefs.TOTAL_COINS, 0);
        tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);

        init();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        b.startAppBanner(this, mainlayout);
        if (CheckNetworkConnection.isConnectionAvailable(this)) {

        } else {
            play_button_view.setVisibility(View.INVISIBLE);
        }

    }

    private void init() {
        submit_odds_tickets_view = (LinearLayout) findViewById(R.id.submit_odds_tickets_view);
        buy_ticketq_view = (LinearLayout) findViewById(R.id.buy_ticketq_view);
        ticket_prize_view = (LinearLayout) findViewById(R.id.ticket_prize_view);
        how_to_play_view = (LinearLayout) findViewById(R.id.how_to_play_view);

        back_view = (TextView) findViewById(R.id.back_view);
        total_tickets = (TextView) findViewById(R.id.total_tickets);
        total_coin = (TextView) findViewById(R.id.total_coin);

        play_button_view = (ImageView) findViewById(R.id.play_button_view);

        submit_odds_tickets_view.setOnTouchListener(this);
        buy_ticketq_view.setOnTouchListener(this);
        ticket_prize_view.setOnTouchListener(this);
        how_to_play_view.setOnTouchListener(this);
        total_coin.setText("Total Coins: " + String.valueOf(coins));
        total_tickets.setText("Total Tickets: " + String.valueOf(tickets));
        back_view.setOnTouchListener(this);

        play_button_view.setOnTouchListener(this);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.ticket_prize_view:
                intent = new Intent(this, zi.class);

                startActivity(intent);

                break;
            case R.id.buy_ticketq_view:

                if (coins < 3000) {
                    showDialog("More Coins", "You must have 3,000 coins to buy a ticket!");
                }

                break;
            case R.id.submit_odds_tickets_view:

                intent = new Intent(this, z.class);

                startActivity(intent);

                break;
            case R.id.how_to_play_view:

                intent = new Intent(this, j.class);

                startActivity(intent);
                break;

            case R.id.play_button_view:


                intent = new Intent(this, r.class);

                startActivity(intent);
                break;

        }
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
