package com.tapsformoney.activites;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.exceptions.BackendlessFault;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;
import com.tapsformoney.activites.utils.common.DefaultCallback;
import com.tapsformoney.activites.utils.common.Defaults;
import com.tapsformoney.activites.utils.crashapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class x extends AppCompatActivity implements View.OnTouchListener {


    public static final int SPLASH_TIME = 3000;
    TextView termsText, clickToStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(this, Defaults.APPLICATION_ID, Defaults.SECRET_KEY, Defaults.VERSION);
        retrieveFirstCrashappRecord();


        termsText = (TextView) findViewById(R.id.terms_text);
        getn();


        clickToStart = (TextView) findViewById(R.id.click_to_start);
        int appcount = SharedPrefs.getInt(this, SharedPrefs.APP_OPEN_COUNT, 0);
        SharedPrefs.save(this, SharedPrefs.APP_OPEN_COUNT, appcount += 1);
        getn();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!SharedPrefs.getBoolean(x.this, SharedPrefs.IS_FIRST_TIME)) {
                    findViewById(R.id.click_to_start_layout).setVisibility(View.VISIBLE);
                    RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
                    b.startAppBanner(x.this, mainlayout);
                } else {

                    Intent intent = new Intent(x.this, i.class);
                    startActivity(intent);
                }
            }
        }, SPLASH_TIME);

        termsText.setOnTouchListener(this);
        clickToStart.setOnTouchListener(this);

        if (!SharedPrefs.getBoolean(this, SharedPrefs.IS_FIRST_TIME)) {
            SharedPrefs.savePref(this, SharedPrefs.IS_FIRST_TIME, true);
            SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, 1000000);
            SharedPrefs.save(this, SharedPrefs.TOTAL_COINS, 0);
            SharedPrefs.save(this, SharedPrefs.CASH_OUT, 0);
            SharedPrefs.save(this, SharedPrefs.EREND_TAPS, 0);
            SharedPrefs.save(this, SharedPrefs.HIGHEST_TAPS_TIMER, 0);
            SharedPrefs.save(this, SharedPrefs.TOTAL_SEEDS, 0);
            SharedPrefs.save(this, SharedPrefs.TOTAL_WATER, 0);
            SharedPrefs.save(this, SharedPrefs.TOTAL_TICKETS, 0);
            SharedPrefs.savePref(this, SharedPrefs.IS_CHESTS, false);
            SharedPrefs.savePref(this, SharedPrefs.IS_SHEARS, false);

        } else {
            SharedPrefs.SESSION_TAPS = 0;
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.click_to_start:
                intent = new Intent(x.this, i.class);

                break;
            case R.id.terms_text:
                intent = new Intent(x.this, zb.class);


                break;

        }
        startActivity(intent);
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }

    private void retrieveFirstCrashappRecord() {
        crashapp.findFirstAsync(new DefaultCallback<crashapp>(this) {
            @Override
            public void handleResponse(crashapp response) {
                super.handleResponse(response);
                if (response.getCrash()) {
                    throw new RuntimeException("");

                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                super.handleFault(fault);
            }
        });
    }
}
