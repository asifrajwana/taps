package com.tapsformoney.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class zd extends Activity implements View.OnTouchListener {


    boolean stoptapping = false;
    boolean disabletapping = false;
    TextView total_taps, taps_countdown_view, watch_count_view, more_view, back_view;
    ImageView tap_view, taptostart_view;
    private AnimationDrawable frameAnimation;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer_game);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();
        init();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
    }

    private void init() {
        back_view = (TextView) findViewById(R.id.back_view);
        more_view = (TextView) findViewById(R.id.more_view);
        tap_view = (ImageView) findViewById(R.id.tap_view);
        taptostart_view = (ImageView) findViewById(R.id.taptostart_view);
        watch_count_view = (TextView) findViewById(R.id.watch_count_view);
        taps_countdown_view = (TextView) findViewById(R.id.taps_countdown_view);
        total_taps = (TextView) findViewById(R.id.total_taps);

        back_view.setOnTouchListener(this);
        more_view.setOnTouchListener(this);
        tap_view.setOnTouchListener(this);
        watch_count_view.setOnTouchListener(this);
        taps_countdown_view.setOnTouchListener(this);

        frameAnimation = (AnimationDrawable) watch_count_view.getBackground();

        //set true if you want to animate only once
        frameAnimation.setOneShot(true);
        updateTaps();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {

        switch (view.getId()) {

            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.more_view:
                Intent it = new Intent(this, ze.class);
                startActivity(it);

                break;

            case R.id.tap_view:

                if (!disabletapping) {
                    if (!stoptapping) {
                        if (!frameAnimation.isRunning()) {
                            startcounter();
                            frameAnimation.start();
                        }
                        taptostart_view.setVisibility(View.GONE);
                        int tapsCount = Integer.parseInt(taps_countdown_view.getText().toString());
                        tapsCount += 1;
                        taps_countdown_view.setText("" + tapsCount);
                        increaseOneTap();
                    } else {


                        updateHighScore();
                        disabletapping = true;
                        frameAnimation.stop();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showDialog(zd.this, "Wow you got " + "" + "taps in 7 seconds!\nChallenge your friends and see if they \ncan even get close to you!");

                            }
                        }, 1500);
                    }
                }

                break;
            case R.id.watch_count_view:


                break;
            case R.id.taps_countdown_view:


                break;
            case R.id.challange_friend:
                share();

                break;
            case R.id.playagain:
                disabletapping = false;

                stoptapping = false;
                taps_countdown_view.setText("" + 0);
                watch_count_view.setBackgroundResource(R.drawable.animlist);
                frameAnimation = (AnimationDrawable) watch_count_view.getBackground();

                //set true if you want to animate only once
                frameAnimation.setOneShot(true);

                watch_count_view.setText(String.valueOf(7));
                dialog.dismiss();
                dialog.cancel();
                break;


        }
    }

    private void startcounter() {
        final Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                int timeSec = Integer.parseInt(watch_count_view.getText().toString());
                if (timeSec != 0) {

                    timeSec -= 1;
                    watch_count_view.setText(String.valueOf(timeSec));
                    handler.postDelayed(this, 1000);
                } else {
                    stoptapping = true;
                    handler.removeCallbacks(this);
                }
            }
        };

        handler.postDelayed(r, 1000);

    }

    private void increaseOneTap() {
        int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.SESSION_TAPS += 1;
        SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= 1);
        SharedPrefs.save(this, SharedPrefs.EREND_TAPS, tapsErned += 1);

        updateTaps();
    }

    private void updateTaps() {
        total_taps.setText("Total Taps : " + SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0));
    }

    public void showDialog(Activity activity, String msg) {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.timerpopup);
        Button playagain = (Button) dialog.findViewById(R.id.playagain);
        Button challengeFriends = (Button) dialog.findViewById(R.id.challange_friend);
        TextView msj = (TextView) dialog.findViewById(R.id.dilog_text);

        msj.setText(msg);
        playagain.setOnTouchListener(this);
        challengeFriends.setOnTouchListener(this);

        dialog.show();

    }

    private void updateHighScore() {
        int lastScore = SharedPrefs.getInt(this, SharedPrefs.HIGHEST_TAPS_TIMER, 0);
        int tapsCount = Integer.parseInt(taps_countdown_view.getText().toString());

        if (tapsCount > lastScore) {

            SharedPrefs.save(this, SharedPrefs.HIGHEST_TAPS_TIMER, tapsCount);

        }


    }

    private void share() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, "Download Taps for Money and earn $15 gift cards just for growing your Money Tree! @tapsformoney https://appsto.re/us/2vwv6.1");
        try {
            this.startActivity(Intent.createChooser(i, "Share..."));
        } catch (android.content.ActivityNotFoundException ex) {
            //Error
            return;
        }
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
