package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chartboost.sdk.Chartboost;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class k extends Activity implements View.OnTouchListener {
    WeakReference<k> weakRef;
    int FARMS;
    Chartboost cb;
    RelativeLayout containter;
    int ernedtaps;
    TextView total_seeds_view, total_farms, info_view, taps_view, back_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land_view);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        weakRef = new WeakReference<k>(this);
        getn();
        total_seeds_view = (TextView) findViewById(R.id.total_seeds_view);
        containter = (RelativeLayout) findViewById(R.id.containter);
        back_view = (TextView) findViewById(R.id.back_view);
        taps_view = (TextView) findViewById(R.id.taps_view);
        info_view = (TextView) findViewById(R.id.info_view);
        total_farms = (TextView) findViewById(R.id.total_farms);

        int seeds = SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0);
        ernedtaps = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        FARMS = SharedPrefs.getInt(this, SharedPrefs.TOTAL_FARMS, 0);
        updataback(FARMS);
        total_seeds_view.setText("Total Seeds: " + String.valueOf(seeds));
        total_farms.setText("Number of Farms: " + String.valueOf(FARMS));

        back_view.setOnTouchListener(this);
        info_view.setOnTouchListener(this);
        taps_view.setOnTouchListener(this);

        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.containter);
        b.startAppBanner(this, mainlayout, false);

        cb = Chartboost.sharedChartboost();
        String appId = getResources().getString(R.string.shartboost_appid);
        String appSignature = getResources().getString(R.string.shartboost_appsignature);
        cb.onCreate(this, appId, appSignature, t.chartBoostDelegate);

        cb.onStart(this);

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.info_view:
                intent = new Intent(this, l.class);
                startActivity(intent);

                break;

            case R.id.taps_view:
                int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);

                ernedtaps += 7;
                FARMS += 1;
                SharedPrefs.save(this, SharedPrefs.EREND_TAPS, ernedtaps);
                total_farms.setText("Number of Farms: " + String.valueOf(FARMS));
                SharedPrefs.save(this, SharedPrefs.TOTAL_FARMS, FARMS);
                SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= 7);
                if (FARMS % 15 == 0) {
                    ImageView common = new ImageView(this);
                    common.setId(23);
                    common.setImageResource(R.drawable.pouch_common);
                    common.setTag(R.drawable.pouch_common);
                    common.setOnTouchListener(this);
                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    parms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                    if (getViewsByTag(containter, R.drawable.pouch_common).size() == 0)
                        containter.addView(common, parms);
                }
                if (FARMS % 20 == 0) {
                    ImageView common = new ImageView(this);
                    common.setId(23);
                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    parms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    common.setImageResource(R.drawable.pouch_rare);
                    common.setTag(R.drawable.pouch_rare);
                    common.setOnTouchListener(this);
                    if (getViewsByTag(containter, R.drawable.pouch_rare).size() == 0)
                        containter.addView(common, parms);

                }
                updataback(FARMS);

                break;
            case 23:

                performPouchOperation(view, view.getTag());
                break;

            case 25:

                performPouchOperation(view, view.getTag());
                break;


        }
    }

    private void updataBG(int FARMSs) {

        int ff = FARMS;


        if (ff % 100 == 0) {
            containter.setBackgroundResource(R.drawable.f9);

        } else if (ff % 90 == 0) {
            containter.setBackgroundResource(R.drawable.f7);

        } else if (ff % 80 == 0) {
            containter.setBackgroundResource(R.drawable.f6);

        } else if (ff % 60 == 0) {
            containter.setBackgroundResource(R.drawable.f5);

        } else if (ff % 50 == 0) {
            containter.setBackgroundResource(R.drawable.f4);

        } else if (ff % 30 == 0) {
            containter.setBackgroundResource(R.drawable.f3);

        } else if (ff % 20 == 0) {
            containter.setBackgroundResource(R.drawable.f2);

        }


    }

    private void updataback(int s) {

        if (s >= 1500) {
            Button common = new Button(this);
            common.setText("Harvest");
            common.setPadding(10, 10, 10, 10);
            common.setId(25);
//            common.setImageResource(R.drawable.harvest);
            common.setTag(R.drawable.harvest);
            common.setOnTouchListener(this);
            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parms.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            if (getViewsByTag(containter, R.drawable.harvest).size() == 0)
                containter.addView(common, parms);
        }

        if (s > 1350012) {
            containter.setBackgroundResource(R.drawable.f11);
        } else if (s > 129500 && s < 135000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 111001 && s < 120000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 100001 && s < 111000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 90001 && s < 100000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 77001 && s < 90000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 63001 && s < 77000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 51241 && s < 63000) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 33741 && s < 51240) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 21741 && s < 33740) {
            containter.setBackgroundResource(R.drawable.f11);

        } else if (s > 15001 && s < 21740) {
            containter.setBackgroundResource(R.drawable.f10);

        } else if (s > 9871 && s < 15000) {
            containter.setBackgroundResource(R.drawable.f9);

        } else if (s > 6541 && s < 9870) {
            containter.setBackgroundResource(R.drawable.f8);

        } else if (s > 4551 && s < 6540) {
            containter.setBackgroundResource(R.drawable.f7);

        } else if (s > 3051 && s < 4550) {
            containter.setBackgroundResource(R.drawable.f6);

        } else if (s > 2151 && s < 3050) {
            containter.setBackgroundResource(R.drawable.f5);

        } else if (s > 1411 && s < 2150) {
            containter.setBackgroundResource(R.drawable.f4);

        } else if (s > 700 && s < 1410) {
            containter.setBackgroundResource(R.drawable.f3);

        } else if (s > 245 && s < 700) {
            containter.setBackgroundResource(R.drawable.f2);

        } else if (s > 0 && s < 245) {
            containter.setBackgroundResource(R.drawable.f1);

        }


    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                        increaseOneTap();
                        updataback(FARMS);
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
        })

                .show();
    }

    private static ArrayList<View> getViewsByTag(ViewGroup root, int tag) {
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }


        }
        return views;
    }

    private void increaseOneTap() {
        int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= 10000);
        SharedPrefs.save(this, SharedPrefs.EREND_TAPS, tapsErned += 10000);
        SharedPrefs.save(this, SharedPrefs.TOTAL_FARMS, FARMS -= 1500);
        total_farms.setText("Number of Farms: " + String.valueOf(FARMS));

        updateTaps();
    }

    void performPouchOperation(View v, Object obj) {
        containter.removeView(v);
        switch ((int) obj) {
            case R.drawable.pouch_common:
                b.showAdss(k.this, true, null, null, null, weakRef, cb);

                increase(3);
                break;
            case R.drawable.pouch_rare:
                b.showAdss(k.this, true, null, null, null, weakRef, cb);

                increase(100);
                break;

            case R.drawable.harvest:
                showDialog("Harvest Farm!", "You want to harvest farm for 10,000 Taps?");

                increase(100);
                break;

        }


    }

    private void increase(int seeds) {
        int water = SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0);

        SharedPrefs.save(this, SharedPrefs.TOTAL_SEEDS, water += seeds);
        updateTaps();


    }

    private void updateTaps() {

        total_seeds_view.setText("Total Seeds: " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0));
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
