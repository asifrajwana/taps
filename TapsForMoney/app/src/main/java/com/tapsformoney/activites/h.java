package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class h extends Activity implements View.OnTouchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_cards);
        getn();
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);

        ImageView amazon, payapl;
        TextView getting_pay, back_view;


        amazon = (ImageView) findViewById(R.id.amazon);
        payapl = (ImageView) findViewById(R.id.payapl);
        getting_pay = (TextView) findViewById(R.id.getting_pay);
        back_view = (TextView) findViewById(R.id.back_view);

        amazon.setOnTouchListener(this);
        payapl.setOnTouchListener(this);
        getting_pay.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view) {


        switch (view.getId()) {
            case R.id.amazon:

                if (SharedPrefs.getInt(h.this, SharedPrefs.CASH_OUT, 0) > 1) {
                    showDialog("Congratulations!", "You have earned 1 gift card.");

                } else if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) < 999999) {
                    showDialog("You Need More taps!", "You need 1 million taps to get a gift card! ");

                } else {
                    showDialog("Congratulations!", "Click the Cash Out button on the main screen");

                }
                break;
            case R.id.payapl:
                if (SharedPrefs.getInt(h.this, SharedPrefs.CASH_OUT, 0) > 1) {
                    showDialog("Congratulations!", "You have earned 1 gift card.");

                } else if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) < 999999) {
                    showDialog("You Need More taps!", "You need 1 million taps to get a gift card! ");

                } else {
                    showDialog("Congratulations!", "Click the Cash Out button on the main screen");

                }
                break;
            case R.id.getting_pay:
                Intent intent = new Intent(h.this, g.class);
                startActivity(intent);
                break;
            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);

                break;

        }

    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
