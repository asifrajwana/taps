package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chartboost.sdk.Chartboost;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.CheckNetworkConnection;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class n extends Activity implements View.OnTouchListener {
    LinearLayout coin_left, coin_right;
    TextView status_view, total_coins, back_view, goagain, quit, more_view;
    ImageView coin_left_image, coin_right_image, grow_farm;
    boolean firstTime = true;
    int correct = 1;
    boolean reachedLimt = false;
    WeakReference<n> weakRef;
    Chartboost cb;
    int increm = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_game);
        init();
        weakRef = new WeakReference<n>(this);
        getn();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.startAppBanner(this, mainlayout);
    }


    private void init() {
        coin_left = (LinearLayout) findViewById(R.id.coin_left);
        coin_right = (LinearLayout) findViewById(R.id.coin_right);
        back_view = (TextView) findViewById(R.id.back_view);
        total_coins = (TextView) findViewById(R.id.title_text);
        status_view = (TextView) findViewById(R.id.status_view);
        more_view = (TextView) findViewById(R.id.more_view);
        quit = (TextView) findViewById(R.id.quit);
        goagain = (TextView) findViewById(R.id.goagain);
        coin_left_image = (ImageView) findViewById(R.id.coin_left_image);
        coin_right_image = (ImageView) findViewById(R.id.coin_right_image);
        grow_farm = (ImageView) findViewById(R.id.grow_farm);
        coin_left.setOnTouchListener(this);
        coin_right.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
//        goagain.setOnTouchListener(this);
        grow_farm.setOnTouchListener(this);
        more_view.setOnTouchListener(this);
//        quit.setOnTouchListener(this);
        updateTaps(SharedPrefs.SESSION_TAPS);
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
        cb = Chartboost.sharedChartboost();
        String appId = getResources().getString(R.string.shartboost_appid);
        String appSignature = getResources().getString(R.string.shartboost_appsignature);
        cb.onCreate(this, appId, appSignature, t.chartBoostDelegate);

        cb.onStart(this);
    }

    private void updateTaps(int increms) {
        int a = SharedPrefs.SESSION_TAPS += increms;
        total_coins.setText("Taps This Session: " + a);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:

                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.gameover:

                findViewById(R.id.gameover).setVisibility(View.GONE);
                findViewById(R.id.table).setVisibility(View.VISIBLE);

                reset();

                break;
            case R.id.quit:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.more_view:
                intent = new Intent(this, q.class);
                startActivity(intent);


                break;
            case R.id.grow_farm:
                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    findViewById(R.id.table).setVisibility(View.VISIBLE);

                    increaseOneTap(increm, 1);
                    grow_farm.setImageResource(R.drawable.moneyf1);
                    grow_farm.setOnTouchListener(null);
                    status_view.setVisibility(View.GONE);
                } else {
                    findViewById(R.id.table).setVisibility(View.INVISIBLE);
                    grow_farm.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.coin_left:

                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    b.showAdss(n.this, null, null, weakRef, null, cb);
                    correct = generateRandom();
                    status_view.setVisibility(View.GONE);
                    coin_left.setVisibility(View.VISIBLE);
                    increaseOneTap(increm, 0);
                } else {
                    findViewById(R.id.table).setVisibility(View.INVISIBLE);
                }


                break;
            case R.id.coin_right:
                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    if (!firstTime) {
                        correct = generateRandom();

                    } else {
                        b.showAdss(n.this, true, null, null, weakRef, null, cb);

                    }
                    b.showAdss(n.this, null, null, weakRef, null, cb);

                    firstTime = false;
                    status_view.setVisibility(View.GONE);
                    coin_left.setVisibility(View.VISIBLE);
                    increaseOneTap(increm, 1);

                } else {
                    findViewById(R.id.table).setVisibility(View.INVISIBLE);
                }

                break;


        }
    }

    private void increaseOneTap(int incremnt, int answer) {

        if (!reachedLimt) {
            if (answer == correct)

            {
                int coins = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
                int totals = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);

                SharedPrefs.save(this, SharedPrefs.EREND_TAPS, coins += increm);
                SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, totals -= increm);
                updateTaps(increm);
                switch (incremnt) {


                    case 1:
                        increm = 2;
                        grow_farm.setImageResource(R.drawable.moneyf1);
                        coin_right_image.setImageResource(R.drawable.tp2);
                        coin_left_image.setImageResource(R.drawable.tp2);
                        break;
                    case 2:
                        increm = 4;
                        grow_farm.setImageResource(R.drawable.moneyf);

                        coin_right_image.setImageResource(R.drawable.tp4);
                        coin_left_image.setImageResource(R.drawable.tp4);
                        break;
                    case 4:
                        increm = 8;
                        grow_farm.setImageResource(R.drawable.moneyf2);

                        coin_right_image.setImageResource(R.drawable.tp8);
                        coin_left_image.setImageResource(R.drawable.tp8);
                        break;
                    case 8:
                        grow_farm.setImageResource(R.drawable.moneyf4);

                        increm = 15;
                        coin_right_image.setImageResource(R.drawable.tp15);
                        coin_left_image.setImageResource(R.drawable.tp15);
                        break;
                    case 15:
                        grow_farm.setImageResource(R.drawable.moneyf5);

                        increm = 20;
                        coin_right_image.setImageResource(R.drawable.tp20);
                        coin_left_image.setImageResource(R.drawable.tp20);
                        break;

                    case 20:
                        grow_farm.setImageResource(R.drawable.moneyf6);

                        increm = 50;
                        coin_right_image.setImageResource(R.drawable.tp50);
                        coin_left_image.setImageResource(R.drawable.tp50);
                        break;
                    case 50:
                        grow_farm.setImageResource(R.drawable.moneyf7);

                        increm = 100;
                        coin_right_image.setImageResource(R.drawable.tp100);
                        coin_left_image.setImageResource(R.drawable.tp100);
                        break;
                    case 100:
                        grow_farm.setImageResource(R.drawable.moneyf8);

                        increm = 170;
                        coin_right_image.setImageResource(R.drawable.tp170);
                        coin_left_image.setImageResource(R.drawable.tp170);
                        break;
                    case 170:
                        grow_farm.setImageResource(R.drawable.moneyf9);

                        increm = 300;
                        coin_right_image.setImageResource(R.drawable.tp300);
                        coin_left_image.setImageResource(R.drawable.tp300);
                        break;
                    case 300:
                        grow_farm.setImageResource(R.drawable.moneyf10);

                        increm = 750;
                        coin_right_image.setImageResource(R.drawable.tp750);
                        coin_left_image.setImageResource(R.drawable.tp750);
                        break;
                    case 750:
                        grow_farm.setImageResource(R.drawable.moneyf11);

                        increm = 1500;
                        coin_right_image.setImageResource(R.drawable.tp1500);
                        coin_left_image.setImageResource(R.drawable.tp1500);
                        break;
                    case 1500:
                        grow_farm.setImageResource(R.drawable.moneyf12);

                        increm = 3000;
                        coin_right_image.setImageResource(R.drawable.tp3000);
                        coin_left_image.setImageResource(R.drawable.tp3000);
                        break;

                    case 3000:
                        grow_farm.setImageResource(R.drawable.moneyf13);

                        increm = 5000;
                        coin_right_image.setImageResource(R.drawable.tp5000);
                        coin_left_image.setImageResource(R.drawable.tp5000);
                        break;
                    case 5000:
                        grow_farm.setImageResource(R.drawable.moneyf14);

                        increm = 8000;
                        coin_right_image.setImageResource(R.drawable.tp8000);
                        coin_left_image.setImageResource(R.drawable.tp8000);
                        break;
                    case 8000:
                        grow_farm.setImageResource(R.drawable.moneyf15);

                        increm = 12000;
                        coin_right_image.setImageResource(R.drawable.tp12000);
                        coin_left_image.setImageResource(R.drawable.tp12000);
                        break;
                    case 12000:
                        grow_farm.setImageResource(R.drawable.moneyf16);

                        increm = 16000;
                        coin_right_image.setImageResource(R.drawable.tp16000);
                        coin_left_image.setImageResource(R.drawable.tp16000);
                        break;
                    case 16000:
                        grow_farm.setImageResource(R.drawable.moneyf17);

                        increm = 25000;
                        coin_right_image.setImageResource(R.drawable.tp25000);
                        coin_left_image.setImageResource(R.drawable.tp25000);
                        break;
                    case 25000:
                        grow_farm.setImageResource(R.drawable.moneyf18);

                        increm = 30000;
                        coin_right_image.setImageResource(R.drawable.tp30000);
                        coin_left_image.setImageResource(R.drawable.tp30000);
                        break;
                    case 30000:
                        grow_farm.setImageResource(R.drawable.moneyf19);

                        increm = 50000;
                        coin_right_image.setImageResource(R.drawable.tp50000);
                        coin_left_image.setImageResource(R.drawable.tp50000);

                        break;
                    case 50000:
                        grow_farm.setImageResource(R.drawable.moneyf20);

                        increm = 50000;

                        findViewById(R.id.table).setVisibility(View.INVISIBLE);

                        reachedLimt = true;
                        break;
                }


            } else {
                status_view.setVisibility(View.VISIBLE);

                status_view.setText("So Close! Try Again!");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status_view.setText("Tap to start!");

                    }
                }, 2000);
                findViewById(R.id.gameover).setVisibility(View.VISIBLE);
                findViewById(R.id.gameover).setOnTouchListener(this);
                findViewById(R.id.table).setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.table).setVisibility(View.INVISIBLE);
        }

    }

    private void reset() {
        grow_farm.setOnTouchListener(this);
        grow_farm.setImageResource(R.drawable.onetap);

        correct = 1;
        firstTime = true;
        coin_left_image.setImageResource(R.drawable.coin1);
        coin_right_image.setImageResource(R.drawable.coin1);
        status_view.setText("Tap to start!");
        status_view.setVisibility(View.VISIBLE);
        findViewById(R.id.table).setVisibility(View.INVISIBLE);

//        coin_left.setVisibility(View.GONE);
        increm = 1;
    }

    private int generateRandom() {
        Random rn = new Random();
        return rn.nextInt(1 - 0 + 1) + 0;
    }

    private int generateAddsRandom() {
        Random rn = new Random();
        return rn.nextInt(1 - 0 + 1) + 0;
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
