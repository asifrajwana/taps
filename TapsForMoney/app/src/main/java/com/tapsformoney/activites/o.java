package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class o extends Activity implements View.OnTouchListener {


    RelativeLayout chests_view, sheares_view;
    TextView back_view, total_seed_view;
    ImageView info1, info2, info3, icon1, icon2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_tree_lab);
        getn();
        init();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
    }

    private void init() {
        chests_view = (RelativeLayout) findViewById(R.id.chests_view);
        sheares_view = (RelativeLayout) findViewById(R.id.sheares_view);
        info3 = (ImageView) findViewById(R.id.info3);
        info2 = (ImageView) findViewById(R.id.info2);
        info1 = (ImageView) findViewById(R.id.info1);
        icon1 = (ImageView) findViewById(R.id.icon1);
        icon2 = (ImageView) findViewById(R.id.icon2);

        back_view = (TextView) findViewById(R.id.back_view);
        total_seed_view = (TextView) findViewById(R.id.total_seed_view);
        chests_view.setOnTouchListener(this);
        sheares_view.setOnTouchListener(this);
        info3.setOnTouchListener(this);
        info2.setOnTouchListener(this);
        info1.setOnTouchListener(this);

        back_view.setOnTouchListener(this);
        total_seed_view.setText("YOU HAVE " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0) + " SEEDS!");

        if (SharedPrefs.getBoolean(this, SharedPrefs.IS_SHEARS)) {
            icon1.setImageResource(R.drawable.lab_chest);
        } else {
            icon1.setImageResource(R.drawable.lab_locktrim);

        }
        if (SharedPrefs.getBoolean(this, SharedPrefs.IS_CHESTS)) {
            icon2.setImageResource(R.drawable.lab_trim);
        } else {
            icon2.setImageResource(R.drawable.lab_lockchest);

        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.info1:
                showDialog("How To Get Seeds", "You can find seeds in seed pouches while watering your Money Tree or working on your Money Farm! you will find Common,rare,and Epic seed pouches that all contain different numbers of seeds!");


                break;
            case R.id.info2:
                showDialog("Shears", "Shears appear when you're watering your Money Tree and you get +25 taps every time you use them!");


                break;
            case R.id.info3:
                showDialog("Chests", "While watering your Money Tree, you can find Chests! Chests can contain anything from seeds, coin, taps, or tickets!");


                break;
            case R.id.chests_view:
                if (!SharedPrefs.getBoolean(this, SharedPrefs.IS_CHESTS)) {
                    int seedsCount = SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0);
                    if (seedsCount < 100) {
                        showDialog("Chests", "You need 100 seeds to unlock Chests!");
                    } else {
                        showDialog("Chests", "Unlock Chests for 100 seeds?", R.string.stringunlockch, 0);

                    }
                } else {
                    showDialog("Taps For Money", "You have unlocked Chests for your Money Tree! Now when you're watering your Money Tree you can find Chests.Chests can contain anything from seeds ,coin,taps , or tickets!");

                }
                break;
            case R.id.sheares_view:

                if (!SharedPrefs.getBoolean(this, SharedPrefs.IS_SHEARS)) {
                    int seedsCount1 = SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0);
                    if (seedsCount1 < 20) {
                        showDialog("Shears", "You need 20 seeds to unlock Shears!");
                    } else {
                        showDialog("Shears", "Unlock Shears for 20 seeds?", R.string.stringunlocksh, 1);

                    }
                } else {
                    showDialog("Taps For Money", "You Have unlocked Shares for your Money Tree! Now when you're watering your Money Tree,shears will randomly appear. When you use the shears you get +25 taps!");

                }
                break;


        }
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void showDialog(String title, final String text, int yes, final int type) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int totalseeds = SharedPrefs.getInt(o.this, SharedPrefs.TOTAL_SEEDS);

                        if (type == 1) {
                            SharedPrefs.savePref(o.this, SharedPrefs.IS_SHEARS, true);
                            SharedPrefs.save(o.this, SharedPrefs.TOTAL_SEEDS, totalseeds -= 20);

                            showDialog("Taps For Money", "You have Unlocked Shears");
                            icon1.setImageResource(R.drawable.lab_chest);

                        } else {
                            SharedPrefs.save(o.this, SharedPrefs.TOTAL_SEEDS, totalseeds -= 100);
                            SharedPrefs.savePref(o.this, SharedPrefs.IS_CHESTS, true);
                            showDialog("Taps For Money", "You have Unlocked Chests");
                            icon2.setImageResource(R.drawable.lab_trim);


                        }
                        total_seed_view.setText("YOU HAVE " + SharedPrefs.getInt(o.this, SharedPrefs.TOTAL_SEEDS, 0) + " SEEDS!");


                    }
                })
                .setNegativeButton(R.string.stringnot, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
