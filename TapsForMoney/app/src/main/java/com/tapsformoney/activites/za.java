package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class za extends Activity implements View.OnTouchListener {

    TextView back_view;
    RelativeLayout moneytree_view, moneyfarm_view, moneyflower_view;

    ImageView icon1, icon2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taps_for_money_lab);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        init();
        getn();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
    }

    private void init() {
        moneytree_view = (RelativeLayout) findViewById(R.id.moneytree_view);
        moneyfarm_view = (RelativeLayout) findViewById(R.id.moneyfarm_view);
        moneyflower_view = (RelativeLayout) findViewById(R.id.moneyflower_view);
        back_view = (TextView) findViewById(R.id.back_view);
        icon2 = (ImageView) findViewById(R.id.icon2);
        icon1 = (ImageView) findViewById(R.id.icon1);


        moneytree_view.setOnTouchListener(this);
        moneyfarm_view.setOnTouchListener(this);
        moneyflower_view.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.moneytree_view:

                intent = new Intent(this, o.class);
                startActivity(intent);

                break;
            case R.id.moneyfarm_view:

                int waters = SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER);
                if (waters >= 5000) {
                    showDialog("Shears", "Unlock Money Flower for 5,000 water?", R.string.stringunlock, 0);

                } else {
                    showDialog("Money Farm Lab", "You must water your Money Tree at least 5,000 times to unlock the Money Farm Lab!");
                }
                break;
            case R.id.moneyflower_view:
                int waterss = SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER);

                if (waterss >= 7500) {

                    showDialog("Shears", "Unlock Money Flower for 7,500 water?", R.string.stringunlock, 1);


                } else {
                    showDialog("Money Flower Lab", "You must water your Money Tree at least 7,500 times and harvest your Money Farm once to unlock the Money Flower Lab!");
                }

                break;


        }
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void showDialog(String title, final String text, int yes, final int type) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        int totalseeds=SharedPrefs.getInt(za.this,SharedPrefs.TOTAL_WATER);

                        if (type == 1) {
                            SharedPrefs.savePref(za.this, SharedPrefs.IS_FLOWER, true);
//                            SharedPrefs.save(za.this,SharedPrefs.TOTAL_SEEDS,totalseeds-=20);

                            showDialog("Taps For Money", "You have Unlocked Money Flower!");
                        } else {
//                            SharedPrefs.save(za.this,SharedPrefs.IS_FARM,totalseeds-=100);
                            SharedPrefs.savePref(za.this, SharedPrefs.IS_CHESTS, true);
                            showDialog("Taps For Money", "You have Unlocked Money Farm!");

                        }


                    }
                })
                .setNegativeButton(R.string.stringnot, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}

