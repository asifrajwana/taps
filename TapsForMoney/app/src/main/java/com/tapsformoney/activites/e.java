package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class e extends Activity implements View.OnTouchListener {

    ImageView image1, image2, image3, image4, image5, image6, image7;
    TextView back_view;
    int selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getn();
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);

        setContentView(R.layout.activity_discover_taps);

        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.image2);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);
        image6 = (ImageView) findViewById(R.id.image6);
        image7 = (ImageView) findViewById(R.id.image7);
        back_view = (TextView) findViewById(R.id.back_view);

        image1.setOnTouchListener(this);
        image2.setOnTouchListener(this);
        image3.setOnTouchListener(this);
        image4.setOnTouchListener(this);
        image5.setOnTouchListener(this);
        image6.setOnTouchListener(this);
        image7.setOnTouchListener(this);
        back_view.setOnTouchListener(this);


        if (SharedPrefs.getBoolean(this, SharedPrefs.IS_1, false)) {
            selected = 1;
            image1.setImageResource(R.drawable.switchon);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchoff);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_2, false)) {
            selected = 2;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchon);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchoff);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_3, false)) {
            selected = 3;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchon);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchoff);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_4, false)) {
            selected = 4;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchon);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchoff);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_5, false)) {
            selected = 5;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchon);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchoff);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_6, false)) {
            selected = 6;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchon);
            image7.setImageResource(R.drawable.switchoff);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_7, false)) {
            selected = 7;
            image1.setImageResource(R.drawable.switchoff);
            image2.setImageResource(R.drawable.switchoff);
            image3.setImageResource(R.drawable.switchoff);
            image4.setImageResource(R.drawable.switchoff);
            image5.setImageResource(R.drawable.switchoff);
            image6.setImageResource(R.drawable.switchoff);
            image7.setImageResource(R.drawable.switchon);

        }


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view) {


        switch (view.getId()) {

            case R.id.image1:

            {
                if (selected == 1) {
                    SharedPrefs.savePref(this, SharedPrefs.IS_1, false);

                    image1.setImageResource(R.drawable.switchoff);
                    selected = -1;
                } else {
                    selected = 1;
                    image1.setImageResource(R.drawable.switchon);

                    image2.setImageResource(R.drawable.switchoff);
                    image3.setImageResource(R.drawable.switchoff);
                    image4.setImageResource(R.drawable.switchoff);
                    image5.setImageResource(R.drawable.switchoff);
                    image6.setImageResource(R.drawable.switchoff);
                    image7.setImageResource(R.drawable.switchoff);

                    SharedPrefs.savePref(this, SharedPrefs.IS_1, true);
                    SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                    SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                    SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                    SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                    SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                    SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                }
            }
            break;
            case R.id.image2:
                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 15000) {
                    if (selected == 2) {
                        selected = -1;
                        image2.setImageResource(R.drawable.switchoff);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);

                    } else {
                        selected = 2;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchon);
                        image3.setImageResource(R.drawable.switchoff);
                        image4.setImageResource(R.drawable.switchoff);
                        image5.setImageResource(R.drawable.switchoff);
                        image6.setImageResource(R.drawable.switchoff);
                        image7.setImageResource(R.drawable.switchoff);

                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, true);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                    }
                } else {
                    showDialog("Get Tapping!", "You must have at least 15,000 taps to unlock the coin button!");
                }
                break;
            case R.id.image3:
                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 25000 && SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) >= 1000) {


                    if (selected == 3) {
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);

                        selected = -1;
                        image3.setImageResource(R.drawable.switchoff);

                    } else {
                        selected = 3;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchoff);
                        image3.setImageResource(R.drawable.switchon);
                        image4.setImageResource(R.drawable.switchoff);
                        image5.setImageResource(R.drawable.switchoff);
                        image6.setImageResource(R.drawable.switchoff);
                        image7.setImageResource(R.drawable.switchoff);

                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, true);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                    }
                } else {
                    showDialog("Get Tapping!", "You must have at least 25,000 taps and at least 1,000 waters to unlock the sport buttons!");
                }
                break;
            case R.id.image4:
                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 50000 && SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) >= 2500) {

                    if (selected == 4) {
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);

                        selected = -1;
                        image4.setImageResource(R.drawable.switchoff);

                    } else {
                        selected = 4;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchoff);
                        image3.setImageResource(R.drawable.switchoff);
                        image4.setImageResource(R.drawable.switchon);
                        image5.setImageResource(R.drawable.switchoff);
                        image6.setImageResource(R.drawable.switchoff);
                        image7.setImageResource(R.drawable.switchoff);

                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, true);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                    }
                } else {
                    showDialog("Get Tapping!", "You must have at least 50,000 taps and at least 25,00 waters to unlock the dessert buttons!");
                }
                break;
            case R.id.image5:
                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 100000) {


                    if (selected == 5) {
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);

                        selected = -1;
                        image5.setImageResource(R.drawable.switchoff);

                    } else {
                        selected = 5;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchoff);
                        image3.setImageResource(R.drawable.switchoff);
                        image4.setImageResource(R.drawable.switchoff);
                        image5.setImageResource(R.drawable.switchon);
                        image6.setImageResource(R.drawable.switchoff);
                        image7.setImageResource(R.drawable.switchoff);


                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, true);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                    }
                } else {
                    showDialog("Get Tapping!", "You must have at least 100,000 taps to unlock the wallet button!");
                }
                break;
            case R.id.image6:

                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 500000) {

                    if (selected == 6) {
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);

                        selected = -1;
                        image6.setImageResource(R.drawable.switchoff);

                    } else {
                        selected = 6;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchoff);
                        image3.setImageResource(R.drawable.switchoff);
                        image4.setImageResource(R.drawable.switchoff);
                        image5.setImageResource(R.drawable.switchoff);
                        image6.setImageResource(R.drawable.switchon);
                        image7.setImageResource(R.drawable.switchoff);


                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, true);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);
                    }
                } else {
                    showDialog("Get Tapping!", "You must have at least 500,000 taps to unlock the trophy button!");
                }
                break;
            case R.id.image7:
                if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS) >= 750000) {


                    if (selected == 7) {
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, false);

                        selected = -1;
                        image7.setImageResource(R.drawable.switchoff);

                    } else {
                        selected = 7;
                        image1.setImageResource(R.drawable.switchoff);
                        image2.setImageResource(R.drawable.switchoff);
                        image3.setImageResource(R.drawable.switchoff);
                        image4.setImageResource(R.drawable.switchoff);
                        image5.setImageResource(R.drawable.switchoff);
                        image6.setImageResource(R.drawable.switchoff);
                        image7.setImageResource(R.drawable.switchon);


                        SharedPrefs.savePref(this, SharedPrefs.IS_1, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_2, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_3, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_4, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_5, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_6, false);
                        SharedPrefs.savePref(this, SharedPrefs.IS_7, true);
                    }
                } else {
                    showDialog("Get Tapping!", "The reason we're here. You're going to need 750,000 to unlock this button.");
                }
                break;
            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);
                break;

        }

    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
