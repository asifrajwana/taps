package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.tapsformoney.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class a extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_aboutactivity);
        getn();
        TextView content_view = (TextView) findViewById(R.id.content_view);
        TextView rule_view = (TextView) findViewById(R.id.rule_view);
        TextView back_view = (TextView) findViewById(R.id.back_view);
        String text = "Taps for money is a free, fun and easy way to get gift cards! you have the ability to earn $15 gift cards by simply playing games. You can also get $15 sent straight to your PayPal account!\n\n" +
                "Here's how it works: Earn taps by playing games and watching videos. Once you reach a million taps, A screen will pop up for you to enter in your email address and what gift card you want. And BAM! Within a month you'll have that e-gift card in your inbox!\n\n" +
                "Hints:\n" +
                "1)   The more taps you have, the less pop-up ads will show.\n" +
                "2)   If you try and complete all the taps in one day, You are going to wear yourself out. Try to do taps in intervals. For example, do 10,000 a day and you'll have $15 in no time!\n" +
                "3)   Play while you are doing things such as watching TV, taking a break during a video game or sitting in a car (obviously not while driving).\n" +
                "\n" +
                "Types of gift cards offered:\n" +
                "1)   Amazon\n" +
                "2)   PayPal\n" +
                "\n" +
                "Note: The gift cards given are in no way affiliated with Google Play. Additionally, the process of giving out the gift cards is in no way affiliated with Google.";
        content_view.setText(text);

        back_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rule_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(a.this, zb.class);
                startActivity(intent);
            }
        });
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
