package com.tapsformoney.activites.utils;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.concurrent.Future;

public class crashapp {
    private Boolean crash;
    private java.util.Date updated;
    private java.util.Date created;
    private String ownerId;
    private String objectId;

    public Boolean getCrash() {
        return crash;
    }

    public void setCrash(Boolean crash) {
        this.crash = crash;
    }

    public java.util.Date getUpdated() {
        return updated;
    }

    public java.util.Date getCreated() {
        return created;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getObjectId() {
        return objectId;
    }


    public crashapp save() {
        return Backendless.Data.of(crashapp.class).save(this);
    }


    public void saveAsync(AsyncCallback<crashapp> callback) {
        Backendless.Data.of(crashapp.class).save(this, callback);
    }

    public Long remove() {
        return Backendless.Data.of(crashapp.class).remove(this);
    }


    public void removeAsync(AsyncCallback<Long> callback) {
        Backendless.Data.of(crashapp.class).remove(this, callback);
    }

    public static crashapp findById(String id) {
        return Backendless.Data.of(crashapp.class).findById(id);
    }


    public static void findByIdAsync(String id, AsyncCallback<crashapp> callback) {
        Backendless.Data.of(crashapp.class).findById(id, callback);
    }

    public static crashapp findFirst() {
        return Backendless.Data.of(crashapp.class).findFirst();
    }


    public static void findFirstAsync(AsyncCallback<crashapp> callback) {
        Backendless.Data.of(crashapp.class).findFirst(callback);
    }

    public static crashapp findLast() {
        return Backendless.Data.of(crashapp.class).findLast();
    }


    public static void findLastAsync(AsyncCallback<crashapp> callback) {
        Backendless.Data.of(crashapp.class).findLast(callback);
    }

    public static BackendlessCollection<crashapp> find(BackendlessDataQuery query) {
        return Backendless.Data.of(crashapp.class).find(query);
    }


    public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<crashapp>> callback) {
        Backendless.Data.of(crashapp.class).find(query, callback);
    }
}