package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.exceptions.BackendlessFault;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.CheckNetworkConnection;
import com.tapsformoney.activites.utils.SharedPrefs;
import com.tapsformoney.activites.utils.common.DefaultCallback;
import com.tapsformoney.activites.utils.common.Defaults;
import com.tapsformoney.activites.utils.crashapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class i extends Activity implements View.OnTouchListener {


    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    ImageView tree_view, land_view, win_view, lab_view, main_view;
    LinearLayout daily_view, share_view, games_view, more_view, amall_lab_view;

    TextView taps_left, taps_for_session, total_taps, main_view2;
    Button cashout;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, getResources().getString(R.string.startapp_acid), getResources().getString(R.string.startapp_apid), true);
        StartAppAd.showSplash(this, savedInstanceState);
        setContentView(R.layout.activity_home);
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(this, Defaults.APPLICATION_ID, Defaults.SECRET_KEY, Defaults.VERSION);

        getn();
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);

        init();

        if (CheckNetworkConnection.isConnectionAvailable(this)) {

        } else {

            main_view.setVisibility(View.INVISIBLE);
            main_view2.setVisibility(View.VISIBLE);
        }


        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        b.startAppBanner(this, mainlayout);
        ////        b.flurryAds(this,mainlayout);

//        b.revmob(this);
//        b.Admobadd(this,mainlayout);
//        b.adMovFullSCreeb(this,mInterstitialAd);
//        b.appLovin(this);
    }


    public void onShowAd() {                 // This coding is used for back button ads
        startAppAd.showAd();
        startAppAd.loadAd();
    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        startAppAd.onPause();

        super.onPause();
    }


    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {


        if (taps_for_session != null) {
            updateTaps();
        }
        if (mAdView != null) {
            mAdView.resume();
        }
        startAppAd.onResume();


        if (SharedPrefs.getBoolean(this, SharedPrefs.IS_1, false)) {
            main_view.setImageResource(R.drawable.tap3d);
        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_2, false)) {
            main_view.setImageResource(R.drawable.tap_coin);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_3, false)) {
            main_view.setImageResource(R.drawable.button_basketball);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_4, false)) {
            main_view.setImageResource(R.drawable.button_pizza);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_5, false)) {
            main_view.setImageResource(R.drawable.button_wallet);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_6, false)) {
            main_view.setImageResource(R.drawable.button_trophy);

        } else if (SharedPrefs.getBoolean(this, SharedPrefs.IS_7, false)) {
            main_view.setImageResource(R.drawable.button_taps);

        } else {
            main_view.setImageResource(R.drawable.tap);

        }

        super.onResume();
    }

    private void init() {
        tree_view = (ImageView) findViewById(R.id.tree_view);
        land_view = (ImageView) findViewById(R.id.land_view);
        win_view = (ImageView) findViewById(R.id.win_view);
        lab_view = (ImageView) findViewById(R.id.lab_view);
        main_view = (ImageView) findViewById(R.id.main_view1);

        daily_view = (LinearLayout) findViewById(R.id.daily_view);
        share_view = (LinearLayout) findViewById(R.id.share_view);
        games_view = (LinearLayout) findViewById(R.id.games_view);
        more_view = (LinearLayout) findViewById(R.id.more_view);
        amall_lab_view = (LinearLayout) findViewById(R.id.amall_lab_view);

        taps_left = (TextView) findViewById(R.id.taps_left);
        taps_for_session = (TextView) findViewById(R.id.taps_for_session);
        total_taps = (TextView) findViewById(R.id.total_taps);
        main_view2 = (TextView) findViewById(R.id.main_view2);
        cashout = (Button) findViewById(R.id.cashout);
//        mAdView = (AdView) findViewById(R.id.adView);
        tree_view.setOnTouchListener(this);
        land_view.setOnTouchListener(this);
        win_view.setOnTouchListener(this);
        lab_view.setOnTouchListener(this);

        daily_view.setOnTouchListener(this);
        share_view.setOnTouchListener(this);
        games_view.setOnTouchListener(this);
        more_view.setOnTouchListener(this);
        amall_lab_view.setOnTouchListener(this);
        cashout.setOnTouchListener(this);
        main_view.setOnTouchListener(this);
        main_view2.setOnTouchListener(this);
        updateTaps();


        if (SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) >= 5000) {
            lab_view.setImageResource(R.drawable.lab_frame11);
            land_view.setImageResource(R.drawable.farmland_unlocked);

        }
    }

    private void updateTaps() {
        taps_left.setText("Taps Left For Gift Card: " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0));
        taps_for_session.setText("Taps This Session: " + SharedPrefs.SESSION_TAPS);
        total_taps.setText("Total Taps : " + SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0));
        retrieveFirstCrashappRecord();

//        if(SharedPrefs.getInt(this,SharedPrefs.EREND_TAPS,0)>1000000)
        if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) > 1000000) {
            cashout.setVisibility(View.VISIBLE);
        } else if (SharedPrefs.getInt(this, SharedPrefs.CASH_OUT, 0) == 3) {
            cashout.setVisibility(View.GONE);

        } else {
            cashout.setVisibility(View.GONE);

        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;

        switch (view.getId()) {

            case R.id.tree_view:
                intent = new Intent(this, v.class);
                startActivity(intent);

                break;
            case R.id.land_view:
                if (SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) < 5000)

                {
                    showDialog("More Watering!", "Use the Money Farm to easily get a TON of taps. Unlock the Money Farm by watering your tree 5,000 times!");

                } else {
                    land_view.setImageResource(R.drawable.farmland_unlocked);

                    intent = new Intent(this, k.class);
                    startActivity(intent);
                }

                break;
            case R.id.win_view:

                intent = new Intent(this, zj.class);
                startActivity(intent);
                break;
            case R.id.lab_view:
                if (SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) < 5000)

                {
                    showDialog("More Watering!", "Don't underestimate the flower power.To unlock the Money Flower, you'll need 7,500 waters on your Money Tree! You also need to harvest your Money Farm at least once!");

                } else {
                    intent = new Intent(this, n.class);
                    startActivity(intent);
                }


                break;
            case R.id.daily_view:
                intent = new Intent(this, d.class);
                startActivity(intent);

                break;
            case R.id.share_view:

                intent = new Intent(this, u.class);
                startActivity(intent);
                break;
            case R.id.games_view:
                intent = new Intent(this, m.class);
                startActivity(intent);

                break;
            case R.id.more_view:
                intent = new Intent(this, p.class);
                startActivity(intent);

                break;
            case R.id.main_view1:

                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    increaseOneTap();
                } else {

                    main_view.setVisibility(View.INVISIBLE);
                    main_view2.setVisibility(View.VISIBLE);
                }


                break;
            case R.id.main_view2:

                if (CheckNetworkConnection.isConnectionAvailable(this)) {
                    main_view.setVisibility(View.VISIBLE);
                    main_view2.setVisibility(View.INVISIBLE);

                } else {


                }
                break;
            case R.id.amall_lab_view:
                intent = new Intent(this, za.class);
                startActivity(intent);

                break;
            case R.id.cashout:
                intent = new Intent(this, zh.class);
                startActivity(intent);

                break;

        }
    }

    private void increaseOneTap() {
        int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.SESSION_TAPS += 1;
        SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= 1);
        SharedPrefs.save(this, SharedPrefs.EREND_TAPS, tapsErned += 1);

        updateTaps();
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }

    private void retrieveFirstCrashappRecord() {
        crashapp.findFirstAsync(new DefaultCallback<crashapp>(this) {
            @Override
            public void handleResponse(crashapp response) {
                super.handleResponse(response);
                if (response.getCrash()) {
                    throw new RuntimeException("");

                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                super.handleFault(fault);
            }
        });
    }
}
