package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tapsformoney.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class p extends Activity implements AdapterView.OnItemClickListener {

    ListView more_list_view;

    String rate = "www.playstore.com";
    String[] values = new String[]{"About",
            "Rate Taps for Money",
            "Share (+20,000 taps)",
            "My Gift Cards"
            , "Unlock Tap Buttons"
            , "Discover Apps (+225 taps)",

            "Main Menu"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FlurryAgent.setLogEnabled(true);
//        FlurryAgent.setLogLevel(Log.VERBOSE);
//        FlurryAgent.setLogEvents(true);
//        // NOTE: Use your own Flurry API key. This is left here to make sample review easier
//        FlurryAgent.init(this, "WPMQMD8JYTWDX4SQYNXR");

        setContentView(R.layout.activity_more);
        getn();
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);

        more_list_view = (ListView) findViewById(R.id.more_list_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        more_list_view.setAdapter(adapter);
        more_list_view.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(this, a.class);
                startActivity(intent);
                break;
            case 1:
                openWeb(rate);
                break;
            case 2:
                intent = new Intent(this, u.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, h.class);
                startActivity(intent);

                break;
            case 4:
                intent = new Intent(this, e.class);
                startActivity(intent);
                break;
            case 5:
                final WeakReference<p> weakRef = new WeakReference<p>(this);

                t rewardedVideos = new t();
                rewardedVideos.loadads(p.this, weakRef);
                break;
            case 6:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);
                break;
            case 7:
                openWeb(rate);

                break;
            case 8:


                break;
        }
    }

    public void openWeb(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
        startActivity(browserIntent);
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
