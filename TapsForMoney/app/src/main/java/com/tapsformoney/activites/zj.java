package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class zj extends Activity implements View.OnTouchListener {

    LinearLayout pizewinner_view, minigame_view, buy_view, submit_view;
    AlertDialog alertDialog;
    TextView back_view, howtowin_view, yes, no, total_tickets;
    int tickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win_prizes);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();
        init();

        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        b.startAppBanner(this, mainlayout);


    }

    private void init() {


        submit_view = (LinearLayout) findViewById(R.id.submit_view);
        buy_view = (LinearLayout) findViewById(R.id.buy_view);
        minigame_view = (LinearLayout) findViewById(R.id.minigame_view);
        pizewinner_view = (LinearLayout) findViewById(R.id.pizewinner_view);
        submit_view = (LinearLayout) findViewById(R.id.submit_view);

        howtowin_view = (TextView) findViewById(R.id.howtowin_view);
        back_view = (TextView) findViewById(R.id.back_view);
        total_tickets = (TextView) findViewById(R.id.total_tickets);


        submit_view.setOnTouchListener(this);
        buy_view.setOnTouchListener(this);
        minigame_view.setOnTouchListener(this);
        pizewinner_view.setOnTouchListener(this);
        submit_view.setOnTouchListener(this);

        howtowin_view.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
        tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);
        total_tickets.setText("Tickets: " + String.valueOf(tickets));
    }


    @Override
    protected void onResume() {
        tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);
        total_tickets.setText("Tickets: " + String.valueOf(tickets));
        super.onResume();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.yes:
                alertDialog.dismiss();
                int totalTaps = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS);
                if (totalTaps >= 5000) {
                    SharedPrefs.save(this, SharedPrefs.TOTAL_TICKETS, tickets += 1);
                    showDialog("Ticket was bought!", "You successfully purchased a ticket! Now click submit to see if you'll be the lucky winner!");
                    tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);
                    total_tickets.setText("Tickets: " + String.valueOf(tickets));
                    SharedPrefs.save(this, SharedPrefs.EREND_TAPS, totalTaps -= 5000);

                } else {

                    showDialog("Better Start Tapping!", "You must have at least 5,000 taps to buy a ticket");

                }

                break;
            case R.id.no:
                alertDialog.dismiss();

                break;


            case R.id.submit_view:

                intent = new Intent(this, z.class);
                startActivity(intent);
                break;
            case R.id.buy_view:

                showBuyTicketDialog();
                break;
            case R.id.howtowin_view:

                intent = new Intent(this, zc.class);
                intent.putExtra("type", 1);
                intent.putExtra("title", "How to Win Prizes");
                intent.putExtra("content", "How The Submission works: \n" +
                        "\n" +
                        "1. You buy tickets using your taps and each ticket is worth 5,000 taps! You can also buy tickets with 3,000 coins that you can earn from playing mini games. \n" +
                        "2. When you submit your tickets, we receive on our end the number of tickets and we will put you in the drawing for your selected prize. (See more information on the prize selecting process below)\n" +
                        "3. You can submit as many tickets as you want! However, when you get over a million taps, you will no longer be able to submit any tickets. \n" +
                        "4. Every day we announce a new prize winner! So make sure you’re checking back in daily to see if you won!\n" +
                        "\n" +
                        "The Selection Process:\n" +
                        "\n" +
                        "1. Lets say you submit 6 tickets. Your email will be listed 6 times.\n" +
                        "2. Lets say you submit 6 tickets, then 4 tickets, then 8 tickets, then 2 tickets. Your total number of tickets is 20 and your email will be listed 20 times. \n" +
                        "3. Your ticket submissions will be put on a numbered list along with everyone else that submitted for the same prize as you. \n" +
                        "4. Once we announce your prize, we run a random number generator that will select a number between the entry numbers. If the number matches up with your email address, you will be the prize winner! We will email you asking for the address that you want us to ship your prize. You can also see if you won by checking in on the app. If you see your name announced as the winner but did not get an email, check your spam folder. We will always send you an email before announcing you as a winner. See Rules and Regulations for more detail.\n" +
                        "\n" +
                        "Current Prize:\n" +
                        "\n" +
                        "1. The prize changes daily! Check out the prizes to see what you could win!\n" +
                        "\n" +
                        "Other:\n" +
                        "\n" +
                        "1. This contest is in no way affiliated with Apple, Inc. \n" +
                        "2. For more information on the contest, click “Rules” below.");

                startActivity(intent);
                break;
            case R.id.minigame_view:
                intent = new Intent(this, m.class);
                startActivity(intent);

                break;
            case R.id.pizewinner_view:

                intent = new Intent(this, zi.class);
                intent.putExtra("isNotDaily", true);
                startActivity(intent);
                break;


        }
    }

    private void showBuyTicketDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.buy_ticker_dialog, null);
        dialogBuilder.setView(dialogView);

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        yes = (TextView) dialogView.findViewById(R.id.yes);
        no = (TextView) dialogView.findViewById(R.id.no);
        yes.setOnTouchListener(this);
        no.setOnTouchListener(this);
        alertDialog.show();
    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
