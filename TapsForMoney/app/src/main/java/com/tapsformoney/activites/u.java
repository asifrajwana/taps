package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class u extends Activity implements View.OnTouchListener {
    ImageView facebook_view, twitter_view, iphone_view, back_view;

    int sh = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secrat);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);

        init();
        getn();
    }

    private void init() {
        iphone_view = (ImageView) findViewById(R.id.iphone_view);
        facebook_view = (ImageView) findViewById(R.id.facebook_view);
        twitter_view = (ImageView) findViewById(R.id.twitter_view);
        back_view = (ImageView) findViewById(R.id.back_view);

        iphone_view.setOnTouchListener(this);
        facebook_view.setOnTouchListener(this);
        twitter_view.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.Admobadd(this, mainlayout);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {

        switch (view.getId()) {

            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;
            case R.id.iphone_view:

                share();
                sh = 1;
                break;

            case R.id.facebook_view:
                sh = 2;
                share();
                break;
            case R.id.twitter_view:
                sh = 3;
                share();
                break;


        }
    }

    private void share() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, "Download Taps for Money and earn $15 gift cards just for growing your Money Tree! @tapsformoney https://play.google.com/store/apps/details?id=com.tapsformoney");
        try {
            this.startActivityForResult(Intent.createChooser(i, "Share..."), 33);
        } catch (android.content.ActivityNotFoundException ex) {
            //Error
            return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (sh) {
            case 1:

                if (!SharedPrefs.getBoolean(u.this, SharedPrefs.SHARE_1, false)) {
                    increaseOneTap(2000);
                    SharedPrefs.savePref(u.this, SharedPrefs.SHARE_1, true);
                }
                break;
            case 2:
                if (!SharedPrefs.getBoolean(u.this, SharedPrefs.SHARE_2, false)) {
                    increaseOneTap(5000);
                    SharedPrefs.savePref(u.this, SharedPrefs.SHARE_2, true);
                }


                break;
            case 3:

                if (!SharedPrefs.getBoolean(u.this, SharedPrefs.SHARE_3, false)) {
                    increaseOneTap(5000);
                    SharedPrefs.savePref(u.this, SharedPrefs.SHARE_3, true);
                }

                break;
            default:
                break;
        }
    }

    private void increaseOneTap(int taps) {

        int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= taps);
        SharedPrefs.save(this, SharedPrefs.EREND_TAPS, tapsErned += taps);

    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
