package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chartboost.sdk.Chartboost;
import com.tapsformoney.R;
import com.tapsformoney.activites.utils.CheckNetworkConnection;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class v extends Activity implements View.OnTouchListener {
    WeakReference<v> weakRef;
    Chartboost cb;

    TextView seedinfo_view, back_view, info_view, count_view, tap_water;
    int currentwater = 0;
    RelativeLayout pochview, backg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seed_game);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();
        b.index = 0;
        weakRef = new WeakReference<v>(this);

        init();
        updataback(SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER, 0));
        cb = Chartboost.sharedChartboost();
        String appId = getResources().getString(R.string.shartboost_appid);
        String appSignature = getResources().getString(R.string.shartboost_appsignature);
        cb.onCreate(this, appId, appSignature, t.chartBoostDelegate);

        cb.onStart(this);

    }

    private void init() {
        count_view = (TextView) findViewById(R.id.count_view);
        info_view = (TextView) findViewById(R.id.info_view);
        back_view = (TextView) findViewById(R.id.back_view);
        seedinfo_view = (TextView) findViewById(R.id.seedinfo_view);
        tap_water = (TextView) findViewById(R.id.tap_water);
        pochview = (RelativeLayout) findViewById(R.id.pochview);
        backg = (RelativeLayout) findViewById(R.id.backg);

        info_view.setOnTouchListener(this);
        back_view.setOnTouchListener(this);
        tap_water.setOnTouchListener(this);
        seedinfo_view.setOnTouchListener(this);
        updateTaps();
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.backg);
        b.startAppBanner(this, mainlayout);
        if (CheckNetworkConnection.isConnectionAvailable(v.this)) {

        } else {
            tap_water.setVisibility(View.INVISIBLE);
        }
    }


    private void updateTaps() {
        count_view.setText("Total waters: " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER, 0) + "\nTotal seeds: " + SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0));
    }

    private void increaseOneTap() {
        int water = SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER, 0);

        SharedPrefs.save(this, SharedPrefs.TOTAL_WATER, water += 1);
        currentwater += 1;
        updateTaps();

        showBag();
    }

    private void showBag() {
        if (currentwater % 15 == 0) {
            ImageView common = new ImageView(this);
            common.setId(23);
            common.setImageResource(R.drawable.pouch_common);
            common.setTag(R.drawable.pouch_common);
            common.setOnTouchListener(this);
            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parms.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            if (getViewsByTag(pochview, R.drawable.pouch_rare).size() == 0)
                pochview.addView(common, parms);
        }
        if (currentwater % 25 == 0) {
            ImageView common = new ImageView(this);
            common.setId(23);
            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            common.setImageResource(R.drawable.pouch_rare);
            common.setTag(R.drawable.pouch_rare);
            common.setOnTouchListener(this);
            if (getViewsByTag(pochview, R.drawable.pouch_rare).size() == 0)
                pochview.addView(common, parms);

        }
        if (currentwater % 75 == 0) {
            ImageView common = new ImageView(this);
            common.setId(23);
            common.setImageResource(R.drawable.pouch_epic);
            common.setTag(R.drawable.pouch_epic);
            common.setOnTouchListener(this);
            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parms.addRule(RelativeLayout.ALIGN_PARENT_LEFT | RelativeLayout.ALIGN_PARENT_BOTTOM);
            if (getViewsByTag(pochview, R.drawable.pouch_epic).size() == 0) {
                pochview.removeAllViews();
                pochview.addView(common, parms);
            }
        }
        if (currentwater % 100 == 0) {
            ImageView common = new ImageView(this);
            common.setId(23);
            common.setImageResource(R.drawable.ferti);
            common.setTag(R.drawable.ferti);
            common.setOnTouchListener(this);
            RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            parms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT | RelativeLayout.ALIGN_PARENT_BOTTOM);
            if (getViewsByTag(pochview, R.drawable.ferti).size() == 0)
                pochview.addView(common, parms);
        }

        if (SharedPrefs.getBoolean(this, SharedPrefs.IS_SHEARS)) {
            if (currentwater % 30 == 0) {
                ImageView common = new ImageView(this);
                common.setId(23);
                common.setImageResource(R.drawable.shear_button);
                common.setTag(R.drawable.shear_button);
                common.setOnTouchListener(this);
                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                parms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT | RelativeLayout.ALIGN_PARENT_BOTTOM);
                if (getViewsByTag(pochview, R.drawable.shear_button).size() == 0)
                    pochview.addView(common, parms);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.back_view:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;

            case R.id.count_view:


                break;

            case R.id.tap_water:

                if (CheckNetworkConnection.isConnectionAvailable(v.this)) {
                    b.showAdss(v.this, weakRef, null, null, null, cb);

                    updataback(currentwater);
                    increaseOneTap();
                    increasetaps(10);
                    if (SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER, 0) > 50000) {
                        tap_water.setVisibility(View.INVISIBLE);

                    }
                } else {
                    tap_water.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.info_view:
                intent = new Intent(this, zc.class);
                intent.putExtra("title", "Money Tree");
                intent.putExtra("content", "Watch your money tree grow as your total taps go up! You can also water your tree to increase your taps! Every time you water your tree you will get +10 taps, but a pop up ad will show as well!\n" +
                        "\n" +
                        "Once you water your tree 5,000 times, you will unlock the Money Farm! Harvest your money farm to skyrocket to a million taps in no time!\n" +
                        "\n" +
                        "While watering your Money Tree, seed pouches will appear. You can find Common , Rare , and Epic seed pouches that all contain different numbers of seeds!\n" +
                        "\n" +
                        "Unlocks available for Money Tree:\n" +
                        "(1) Shears (20 seeds)\n" +
                        "(2) Chests (100 seeds)");
                intent.putExtra("type", 0);
                startActivity(intent);

                break;
            case R.id.seedinfo_view:
                intent = new Intent(this, w.class);
                startActivity(intent);

                break;

            case 23:

                performPouchOperation(view, view.getTag());
                break;

        }
    }

    void performPouchOperation(View v, Object obj) {
        pochview.removeView(v);
        switch ((int) obj) {
            case R.drawable.pouch_common:
                b.showApplovin(com.tapsformoney.activites.v.this);

                increase(3);
                break;
            case R.drawable.pouch_epic:
                b.showApplovin(com.tapsformoney.activites.v.this);

                increase(100);
                break;
            case R.drawable.pouch_rare:
                b.showApplovin(com.tapsformoney.activites.v.this);


                increase(35);
                break;
            case R.drawable.ferti:
                b.showApplovin(com.tapsformoney.activites.v.this);

                increasetaps(225);
                break;
            case R.drawable.shear_button:
//                b.showApplovin(v.this);
                t rewardedVideos = new t();
                rewardedVideos.chartboostaddshowing(com.tapsformoney.activites.v.this, cb);
                increasetaps(25);
                break;

        }


    }

    private void increase(int seeds) {
        int water = SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS, 0);

        SharedPrefs.save(this, SharedPrefs.TOTAL_SEEDS, water += seeds);
        updateTaps();


    }

    private void increasetaps(int seeds) {
        int tapsLeft = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TAPS, 0);
        int tapsErned = SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0);
        SharedPrefs.SESSION_TAPS += seeds;
        SharedPrefs.save(this, SharedPrefs.TOTAL_TAPS, tapsLeft -= seeds);
        SharedPrefs.save(this, SharedPrefs.EREND_TAPS, tapsErned += seeds);


    }

    private static ArrayList<View> getViewsByTag(ViewGroup root, int tag) {
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }


        }
        return views;
    }

    private void updataback(int s) {

        if (s > 1350012) {
            backg.setBackgroundResource(R.drawable.tree_20);
        } else if (s > 129500 && s < 135000) {
            backg.setBackgroundResource(R.drawable.tree_19);

        } else if (s > 111001 && s < 120000) {
            backg.setBackgroundResource(R.drawable.tree_18);

        } else if (s > 100001 && s < 111000) {
            backg.setBackgroundResource(R.drawable.tree_17);

        } else if (s > 90001 && s < 100000) {
            backg.setBackgroundResource(R.drawable.tree_16);

        } else if (s > 77001 && s < 90000) {
            backg.setBackgroundResource(R.drawable.tree_15);

        } else if (s > 63001 && s < 77000) {
            backg.setBackgroundResource(R.drawable.tree_14);

        } else if (s > 51241 && s < 63000) {
            backg.setBackgroundResource(R.drawable.tree_13);

        } else if (s > 33741 && s < 51240) {
            backg.setBackgroundResource(R.drawable.tree_12);

        } else if (s > 21741 && s < 33740) {
            backg.setBackgroundResource(R.drawable.tree_11);

        } else if (s > 15001 && s < 21740) {
            backg.setBackgroundResource(R.drawable.tree_10);

        } else if (s > 9871 && s < 15000) {
            backg.setBackgroundResource(R.drawable.tree_09);

        } else if (s > 6541 && s < 9870) {
            backg.setBackgroundResource(R.drawable.tree_08);

        } else if (s > 4551 && s < 6540) {
            backg.setBackgroundResource(R.drawable.tree_07);

        } else if (s > 3051 && s < 4550) {
            backg.setBackgroundResource(R.drawable.tree_06);

        } else if (s > 2151 && s < 3050) {
            backg.setBackgroundResource(R.drawable.tree_05);

        } else if (s > 1411 && s < 2150) {
            backg.setBackgroundResource(R.drawable.tree_04);

        } else if (s > 700 && s < 1410) {
            backg.setBackgroundResource(R.drawable.tree_03);

        } else if (s > 245 && s < 700) {
            backg.setBackgroundResource(R.drawable.tree_02);

        } else if (s > 0 && s < 245) {
            backg.setBackgroundResource(R.drawable.tree_01);

        }


    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }

}
