package com.tapsformoney.activites.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by salman on 8/27/2016.
 */
public class SharedPrefs {

    //SharedPreferences file name
    private static String SHARED_PREFS_FILE_NAME = "tapForMoney";

    //here you can centralize all your shared prefs keys
    public static String TOTAL_TAPS = "TOTAL_TAPS";
    public static String CASH_OUT = "CASH_OUT";
    public static String TOTAL_COINS = "TOTAL_COINS";
    public static String TOTAL_TICKETS = "TOTAL_TICKETS";
    public static String APP_OPEN_COUNT = "APP_OPEN_COUNT";
    public static int SESSION_TAPS = 0;
    public static String EREND_TAPS = "EREND_TAPS";
    public static String IS_FIRST_TIME = "IS_FIRST_TIME";
    public static String HIGHEST_TAPS_TIMER = "HIGHEST_TAPS_TIMER";
    public static String TOTAL_WATER = "TOTAL_WATER";
    public static String TOTAL_SEEDS = "TOTAL_SEEDS";
    public static String TOTAL_FARMS = "TOTAL_FARMS";
    public static String IS_SHEARS = "IS_SHEARS";
    public static String IS_CHESTS = "IS_CHESTS";
    public static String IS_FLOWER = "IS_FLOWER";
    public static String IS_FARM = "IS_FARM";
    public static String IS_1 = "IS_1";
    public static String IS_2 = "IS_2";
    public static String IS_3 = "IS_3";
    public static String IS_4 = "IS_4";
    public static String IS_5 = "IS_5";
    public static String IS_6 = "IS_6";
    public static String IS_7 = "IS_7";
    public static String SHARE_1 = "SHARE_1";
    public static String SHARE_2 = "SHARE_2";
    public static String SHARE_3 = "SHARE_3";
    //get the SharedPreferences object instance
    //create SharedPreferences file if not present


    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    //Save Booleans
    public static void savePref(Context context, String key, boolean value) {
        getPrefs(context).edit().putBoolean(key, value).commit();
    }

    //Get Booleans
    public static boolean getBoolean(Context context, String key) {
        return getPrefs(context).getBoolean(key, false);
    }

    //Get Booleans if not found return a predefined default value
    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        return getPrefs(context).getBoolean(key, defaultValue);
    }

    //Strings
    public static void save(Context context, String key, String value) {
        getPrefs(context).edit().putString(key, value).commit();
    }

    public static String getString(Context context, String key) {
        return getPrefs(context).getString(key, "");
    }

    public static String getString(Context context, String key, String defaultValue) {
        return getPrefs(context).getString(key, defaultValue);
    }

    //Integers
    public static void save(Context context, String key, int value) {
        getPrefs(context).edit().putInt(key, value).commit();
    }

    public static int getInt(Context context, String key) {
        return getPrefs(context).getInt(key, 0);
    }

    public static int getInt(Context context, String key, int defaultValue) {
        return getPrefs(context).getInt(key, defaultValue);
    }

    //Floats
    public static void save(Context context, String key, float value) {
        getPrefs(context).edit().putFloat(key, value).commit();
    }

    public static float getFloat(Context context, String key) {
        return getPrefs(context).getFloat(key, 0);
    }

    public static float getFloat(Context context, String key, float defaultValue) {
        return getPrefs(context).getFloat(key, defaultValue);
    }

    //Longs
    public static void save(Context context, String key, long value) {
        getPrefs(context).edit().putLong(key, value).commit();
    }

    public static long getLong(Context context, String key) {
        return getPrefs(context).getLong(key, 0);
    }

    public static long getLong(Context context, String key, long defaultValue) {
        return getPrefs(context).getLong(key, defaultValue);
    }

    //StringSets
    public static void save(Context context, String key, Set<String> value) {
        getPrefs(context).edit().putStringSet(key, value).commit();
    }

    public static Set<String> getStringSet(Context context, String key) {
        return getPrefs(context).getStringSet(key, null);
    }

    public static Set<String> getStringSet(Context context, String key, Set<String> defaultValue) {
        return getPrefs(context).getStringSet(key, defaultValue);
    }

}