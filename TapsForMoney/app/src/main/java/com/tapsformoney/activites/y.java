package com.tapsformoney.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class y extends Activity implements View.OnTouchListener {

    TextView done_submit;
    WebView webview;
    int tickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_ticket);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();
        init();
        ((TextView) findViewById(R.id.gonei)).setText("You must enter your email in the form below , if a form does not appear right away, Do not leave this page, Wait for the form to load.");
        ;
        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
//        b.startAppBanner(this,mainlayout);
    }

    private void init() {
        done_submit = (TextView) findViewById(R.id.done_submit);
        webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);

        done_submit.setOnTouchListener(this);
        tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);

        loadWEbview(tickets);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.done_submit:
                showDialog("Warning!", "Do you want to go back?");

                break;


        }
    }

    @Override
    public void onBackPressed() {

        showDialog("Warning!", "Do you want to go back?");

    }

    private void loadWEbview(int tickets) {
        String URl = "";
        if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) > 1000000 && SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) < 1020000 && SharedPrefs.getInt(this, SharedPrefs.TOTAL_WATER) > 300 && SharedPrefs.getInt(this, SharedPrefs.TOTAL_SEEDS) > 0 && SharedPrefs.getInt(this, SharedPrefs.APP_OPEN_COUNT, 0) > 25) {
            URl = "https://docs.google.com/forms/d/e/1FAIpQLSeFJKRO_0ac7S8Lvtq_FwTb3I3Ota4BUSE-QF2HuU3-kdE-AA/viewform";
        } else if (SharedPrefs.getInt(this, SharedPrefs.EREND_TAPS, 0) > 1000000) {
            URl = "https://docs.google.com/forms/d/e/1FAIpQLSecK5HJ9MlGJm_XbG_nItuYtt-WCWDlCZO7nE6XtXUQabEVxQ/viewform";
        }
        SharedPrefs.save(y.this, SharedPrefs.EREND_TAPS, 0);

        webview.loadUrl(URl);
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }


        });

    }

    private void showDialog(String title, String text) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(R.string.stringyes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        SharedPrefs.save(y.this, SharedPrefs.TOTAL_TICKETS, 0);
                        finish();
                        overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);

                    }
                }).setNegativeButton(R.string.stringno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
        })

                .show();
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
