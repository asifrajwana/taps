package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;

import com.chartboost.sdk.Chartboost;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.banner.Banner;
import com.tapsformoney.R;

import java.lang.ref.WeakReference;
import java.util.Random;

/**
 * Created by salman on 8/30/2016.
 */
public class b {


    static RevMob revmob;
    static Activity currentActivity; // for anonymous classes
    static RevMobFullscreen fullscreen;
    RevMobBanner banner;
    static boolean fullscreenLoaded = false;

    public static int index = 0;
    private static AppLovinInterstitialAdDialog interstitialAdDialog;

    public static void adMovFullSCreeb(final Context context, InterstitialAd mInterstitialAd)

    {

        mInterstitialAd = new InterstitialAd(context);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(context.getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);

        final InterstitialAd finalMInterstitialAd = mInterstitialAd;
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial(finalMInterstitialAd);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
    }

    private static void showInterstitial(InterstitialAd mInterstitialAd) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public static void Admobadd(final Context context, RelativeLayout mainLayout) {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        AdView mAdView = new AdView(context);

        mAdView.setAdUnitId(context.getString(R.string.banner_home_footer));
        mAdView.setAdSize(AdSize.BANNER);
        RelativeLayout.LayoutParams bannerParameters =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);


        // Add the banner to the main layout
        mainLayout.addView(mAdView, bannerParameters);
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                // Check the LogCat to get your test device ID
//                .addTestDevice("9615CB61DF3A18A23E3393EA60619BDD")
//                .build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdOpened() {
            }
        });

    }


    public static void startAppBanner(Context context, RelativeLayout mainLayout) {
        Banner startAppBanner = new Banner(context);
        RelativeLayout.LayoutParams bannerParameters =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        // Add the banner to the main layout
        mainLayout.addView(startAppBanner, bannerParameters);
    }

    public static void startAppBanner(Context context, RelativeLayout mainLayout, boolean d) {
        Banner startAppBanner = new Banner(context);
        RelativeLayout.LayoutParams bannerParameters =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        // Add the banner to the main layout
        mainLayout.addView(startAppBanner, bannerParameters);
    }


    public static void showAdss(Context context, WeakReference<v> weakRef3, WeakReference<r> weakRef2, WeakReference<n> weakRef1, WeakReference<k> weakRef, Chartboost cb) {

        int rand = generateRandom();
        if (rand == 1 || rand == 3) {
            InterstitialAd mInterstitialAd = null;
            StartAppAd startAppAd;
            if (index == 6) {
                index = 0;
            }
            switch (index) {

                case 0:
                    startAppAd = new StartAppAd(context);
                    startAppAd.loadAd();
                    startAppAd.onBackPressed();

                    break;

                case 1:
                    b.adMovFullSCreeb(context, mInterstitialAd);
                    break;
                case 2:
                    showApplovin(context);

                    break;

                case 3:
                    showLoadedFullscreen(context);

                    break;
                case 4:
                    t rewardedVideos = new t();
                    rewardedVideos.chartboostaddshowing(context, cb);
                    break;
                case 5:

                    showApplovin(context);

                    break;

            }

            index++;
        }


        //                    b.startAppBanner(this,backg);
//        b.Admobadd(context,mainlayout);
//                    b.adMovFullSCreeb(this,mInterstitialAd);
    }

    private static int generateRandom() {
        Random rn = new Random();
        int num = rn.nextInt(3 - 0 + 1) + 0;
        Log.d("ads random number", "random num" + String.valueOf(num));
        return num;
    }

    public static void showAdss(Context context, boolean shownow, WeakReference<v> weakRef3, WeakReference<r> weakRef2, WeakReference<n> weakRef1, WeakReference<k> weakRef, Chartboost cb) {

        int rand = generateRandom();
        if (shownow) {
            InterstitialAd mInterstitialAd = null;
            StartAppAd startAppAd;
            if (index == 6) {
                index = 0;
            }
            switch (index) {

                case 0:
                    startAppAd = new StartAppAd(context);
                    startAppAd.loadAd();
                    startAppAd.onBackPressed();

                    break;

                case 1:
                    b.adMovFullSCreeb(context, mInterstitialAd);
                    break;
                case 2:
                    showApplovin(context);
                    break;

                case 3:
                    showLoadedFullscreen(context);

                    break;
                case 4:
                    t rewardedVideos = new t();
                    rewardedVideos.chartboostaddshowing(context, cb);

                    break;
                case 5:

                    showLoadedFullscreen(context);

                    break;

            }

            index++;
        }


        //                    b.startAppBanner(this,backg);
//        b.Admobadd(context,mainlayout);
//                    b.adMovFullSCreeb(this,mInterstitialAd);
    }

    public static void showApplovin(Context context) {
        final AppLovinSdk sdk = AppLovinSdk.getInstance(context);

        if (AppLovinInterstitialAd.isAdReadyToDisplay(((Activity) context))) {
            interstitialAdDialog = AppLovinInterstitialAd.create(sdk, ((Activity) context));

            //
            // Optional: Set ad load, ad display, ad click, and ad video playback callback listeners
            //
            interstitialAdDialog.setAdLoadListener(new AppLovinAdLoadListener() {
                @Override
                public void adReceived(AppLovinAd appLovinAd) {
                    log("Interstitial loaded");
                }

                @Override
                public void failedToReceiveAd(int errorCode) {

                    // Look at AppLovinErrorCodes.java for list of error codes

                    if (errorCode == AppLovinErrorCodes.NO_FILL) {
                        log("No-fill: No ads are currently available for this device/country");
                    } else {
                        log("Interstitial failed to load with error code " + errorCode);
                    }
                }
            });

            interstitialAdDialog.setAdDisplayListener(new AppLovinAdDisplayListener() {
                @Override
                public void adDisplayed(AppLovinAd appLovinAd) {
                    log("Interstitial Displayed");
                }

                @Override
                public void adHidden(AppLovinAd appLovinAd) {

                }
            });

            interstitialAdDialog.setAdClickListener(new AppLovinAdClickListener() {
                @Override
                public void adClicked(AppLovinAd appLovinAd) {
                    log("Interstitial Clicked");
                }
            });

            // This will only ever be used if you have video ads enabled.
            interstitialAdDialog.setAdVideoPlaybackListener(new AppLovinAdVideoPlaybackListener() {
                @Override
                public void videoPlaybackBegan(AppLovinAd appLovinAd) {
                    log("Video Started");
                }

                @Override
                public void videoPlaybackEnded(AppLovinAd appLovinAd, double percentViewed, boolean wasFullyViewed) {
                    log("Video Ended");
                }
            });

                    /*
                     NOTE: We recommend the use of placements (AFTER creating them in your dashboard):

                     interstitialAdDialog.showAndRender(currentAd, "MANUAL_LOADING_SCREEN");

                     To learn more about placements, check out https://applovin.com/integration#androidPlacementsIntegration
                    */
            interstitialAdDialog.show();
        } else {
            // Ideally, the SDK preloads ads when you initialize it in your launch activity
            // you can manually load an ad as demonstrated in InterstitialManualLoadingActivity
            log("Interstitial not ready for display.\nPlease check SDK key or internet connection.");
        }
    }

    private static void log(String log) {
        Log.e("ye lo nenw log", "wakhra " + log);
    }

    public static void startRevMobSession(Context ctx) {
        currentActivity = (Activity) ctx;

        revmob = RevMob.startWithListener(currentActivity, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionStarted() {
                loadFullscreen();
            }

            @Override
            public void onRevMobSessionNotStarted(String message) {
            }
        });
        //Since RevMob session has been started on the first example, our listeners won't be fired again


        //loadStartingAds() is the same code block that is read on our onRevMobSessionIsStarted listener above
        //We'll call it here to emulate onRevMobSessionIsStarted's behaviour on our App
        //You can remove loadStartingAds() from here and use it on onRevMobSessionIsStarted
        loadFullscreen();
    }

    public static void loadFullscreen() {
        fullscreen = revmob.createFullscreen(currentActivity, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                fullscreenLoaded = true;
            }

            @Override
            public void onRevMobAdNotReceived(String message) {
            }

            @Override
            public void onRevMobAdDismissed() {
            }

            @Override
            public void onRevMobAdClicked() {
            }

            @Override
            public void onRevMobAdDisplayed() {
            }

        });
    }

    public static void showLoadedFullscreen(Context v) {
        if (fullscreenLoaded)
            fullscreen.show();
        else
            startRevMobSession(v);
    }

}




