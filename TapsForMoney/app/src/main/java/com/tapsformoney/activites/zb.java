package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.tapsformoney.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class zb extends Activity implements View.OnTouchListener {


    TextView policyText, termsText, mainText;
    String privacy;
    String terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();

        mainText = (TextView) findViewById(R.id.main_text);
        termsText = (TextView) findViewById(R.id.terms_text);
        policyText = (TextView) findViewById(R.id.policy_text);

        policyText.setOnTouchListener(this);
        termsText.setOnTouchListener(this);
        mainText.setOnTouchListener(this);
        privacy = "TAPS FOR MONEY\n" +
                "PRIVACY POLICY\n" +
                "© 2015 APPS THAT PAY, LLC\n" +
                "\n" +
                "What information do we collect?\n" +
                "\n" +
                "We collect information from you after you reach a million taps. You will be asked to provide an email address so we know where to send your gift card. After you fill out your submission, you may be asked to provide a legal form of picture identification, such as a driver’s license, student ID, or a passport. This will never be shared with anyone. The sole purpose of this is to prevent multiple submissions from the same purpose.\n" +
                "\n" +
                "What do we use your information for?\n" +
                "\n" +
                "We need the email to know where to send the gift card. The photo identification is needed to prevent a single person from submitting multiple gift cards. \n" +
                "\n" +
                "How do we protect your information?\n" +
                "\n" +
                "We implement a variety of security measures to maintain the security of your information. All of your information is discarded after 30 days of receiving it. Only the emails are kept on record, but this is solely for record keeping purposes. \n" +
                "\n" +
                "Do we disclose any information to outside parties?\n" +
                "\n" +
                "We do not sell, trade, or otherwise transfer to outside parties your identifiable information. This does not include trusted third parties who assist us in servicing you, so long as those parties agree to keep your information confidential. We may also release your information when we believe release is appropriate to comply with the law. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses (this information may include number of downloads, retention rate, app usage, etc.). \n" +
                "\n" +
                "Third Parties\n" +
                "\n" +
                "Third party links are used on our app at our discretion.  These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content or activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites. \n" +
                "\n" +
                "Terms of Service\n" +
                "\n" +
                "Please visit our terms of service that establishes the use, disclaimers, and limitations of liability governing the use of app at tapsformoney.weebly.com.\n" +
                "\n" +
                "Your consent\n" +
                "\n" +
                "By using Taps for Money, you consent to our privacy policy.\n" +
                "\n" +
                "Contacting us\n" +
                "\n" +
                "If there is any need to contact us, please visit us at our website.";
        terms = "TAPS FOR MONEY\n" +
                "TERMS OF SERVICE\n" +
                "© 2015-2016 APPS THAT PAY, LLC\n" +
                "\n" +
                "Taps for Money is a mobile application for iOS that allows users to increase their “total taps” to one million in order to receive a $15 eGift Card via email. After reaching one million taps, a button will appear that allows the user to submit their email address. Before you reach the submission form, a warning will say the following:\n" +
                "\n" +
                "“If you click “next” then you will not be able to return to this screen! You only have one chance to request a gift card, if you mess your chance up, you will have to tap a million more times to get the gift card.  Here is what you need to do before hitting next:\n" +
                "\n" +
                "1.\tMake sure you have a Google account (this is required to sign in and record your response, this way you can only request one card).\n" +
                "2.\tBe prepared to provide an email address that you have access to. This is where the gift card will be emailed. \n" +
                "3.\tMake sure that your device is connected to the Internet.\n" +
                "\n" +
                "Please Note: If you incorrectly type in your email address, you will not be able to change your submission and will not receive a gift card.”\n" +
                "\n" +
                "Failure to oblige to the above warning will result in a termination of the user receiving his or her gift card. For any reason that your gift card is not sent to you, you will not be notified that you will not receive your gift card, nor will you receive any email. \n" +
                "\n" +
                "Any form of fraud will result in the user not receiving his or her gift card. Any suspicion of fraud will result in the user not receiving his or her gift card.\n" +
                "\n" +
                "Each user is allowed only one gift card. Any duplicating emails during submission will be considered fraud and will result in a termination of your gift card being sent to you.";
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {
            case R.id.main_text:
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);

                break;
            case R.id.terms_text:
                intent = new Intent(zb.this, s.class);
                intent.putExtra("data", terms);
                startActivity(intent);
                break;
            case R.id.policy_text:

                intent = new Intent(zb.this, s.class);
                intent.putExtra("data", privacy);

                startActivity(intent);

                break;

        }
    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
