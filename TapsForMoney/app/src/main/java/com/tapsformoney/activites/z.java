package com.tapsformoney.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tapsformoney.R;
import com.tapsformoney.activites.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class z extends Activity implements View.OnTouchListener {

    TextView done_submit;
    WebView webview;
    int tickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_ticket);
        overridePendingTransition(R.anim.up_slide_in, R.anim.sliding);
        getn();
        init();

        RelativeLayout mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        b.startAppBanner(this, mainlayout);
    }

    private void init() {
        done_submit = (TextView) findViewById(R.id.done_submit);
        webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);

        done_submit.setOnTouchListener(this);
        tickets = SharedPrefs.getInt(this, SharedPrefs.TOTAL_TICKETS, 0);

        loadWEbview(tickets);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.2f);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.setAlpha(1.0f);
            callScreen(v);
            return true;
        }
        return false;

    }

    private void callScreen(View view)

    {
        Intent intent;
        switch (view.getId()) {


            case R.id.done_submit:
                SharedPrefs.save(this, SharedPrefs.TOTAL_TICKETS, 0);
                finish();
                overridePendingTransition(R.anim.sliding, R.anim.down_slide_out);


                break;


        }
    }

    private void loadWEbview(int tickets) {
        String URl = "";
        if (tickets == 1) {
            URl = "https://docs.google.com/forms/d/1F0nZHEKkoikH_3owsxhg9rPHBaI8h28m8BKek7NT7v8/viewform?usp=send_form";
        }
        if (tickets == 2) {
            URl = "https://docs.google.com/forms/d/1SPn-CSSk0_XSTBtNZa4H9_2pIammfLMNYYoEIrz5vWw/viewform?usp=send_form";
        }
        if (tickets == 3) {
            URl = "https://docs.google.com/forms/d/1d-GApPj28xx2ColE5beVVX2Mg1BwdwV-BzrenQ4Pz2w/viewform?usp=send_form";
        }
        if (tickets == 4) {
            URl = "https://docs.google.com/forms/d/1m3Tlj00dTCRUu6H8c-qWn1qbbQhS5bFY57vdPOFl-qQ/viewform?usp=send_form";
        }
        if (tickets == 5) {
            URl = "https://docs.google.com/forms/d/1RKLfUgOtBjPItsyLhffyorTL838flr_2h4B3QxEGZ4Y/viewform?usp=send_form";
        }
        if (tickets == 6) {
            URl = "https://docs.google.com/forms/d/17ZymovA_uJA5P2sqE5C5md9xYrOTbtcHkbavXo6BKBM/viewform?usp=send_form";
        }
        if (tickets == 7) {
            URl = "https://docs.google.com/forms/d/1oEqFqhol-v-QDRusDyzh8hvlSS4CCg47CC1SUYUswm0/viewform?usp=send_form";
        }
        if (tickets == 8) {
            URl = "https://docs.google.com/forms/d/1VhwPud5Q0Zb5RA18lCbxqQ4ujqZ6OUeUKO80rS7dq8s/viewform?usp=send_form";
        }
        if (tickets == 9) {
            URl = "https://docs.google.com/forms/d/1nJ3u_5pOwHrCNCZhyBTh7tLkPIP4-CZzSi0k5vWzC9c/viewform?usp=send_form";
        }
        if (tickets > 9) {
            URl = "https://docs.google.com/forms/d/1xr1yhJ0YQ51ftsob-mKmPcwXVZLCLxdVfqHWoY4ZuEU/viewform?usp=send_form";
        }
        webview.loadUrl(URl);
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }


        });

    }

    private void getn() {
        List<String> acName = new ArrayList<>();

        acName.add("com.tapsformoney.activites.a");
        acName.add("com.tapsformoney.activites.b");
        acName.add("com.tapsformoney.activites.c");
        acName.add("com.tapsformoney.activites.d");
        acName.add("com.tapsformoney.activites.e");
        acName.add("com.tapsformoney.activites.f");
        acName.add("com.tapsformoney.activites.g");
        acName.add("com.tapsformoney.activites.h");
        acName.add("com.tapsformoney.activites.i");
        acName.add("com.tapsformoney.activites.j");
        acName.add("com.tapsformoney.activites.k");
        acName.add("com.tapsformoney.activites.l");
        acName.add("com.tapsformoney.activites.m");
        acName.add("com.tapsformoney.activites.n");
        acName.add("com.tapsformoney.activites.o");
        acName.add("com.tapsformoney.activites.p");
        acName.add("com.tapsformoney.activites.q");
        acName.add("com.tapsformoney.activites.r");
        acName.add("com.tapsformoney.activites.s");
        acName.add("com.tapsformoney.activites.t");
        acName.add("com.tapsformoney.activites.u");
        acName.add("com.tapsformoney.activites.v");
        acName.add("com.tapsformoney.activites.w");
        acName.add("com.tapsformoney.activites.x");
        acName.add("com.tapsformoney.activites.y");
        acName.add("com.tapsformoney.activites.z");
        acName.add("com.tapsformoney.activites.za");
        acName.add("com.tapsformoney.activites.zb");
        acName.add("com.tapsformoney.activites.zc");
        acName.add("com.tapsformoney.activites.zd");
        acName.add("com.tapsformoney.activites.ze");
        acName.add("com.tapsformoney.activites.zf");
        acName.add("com.tapsformoney.activites.zh");
        acName.add("com.tapsformoney.activites.zi");
        acName.add("com.tapsformoney.activites.zj");
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(
                    "com.tapsformoney", PackageManager.GET_ACTIVITIES);

            ArrayList<ActivityInfo> mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));

            for (int x = 0; x < mActivities.size(); x++) {
                Log.e("tag", mActivities.get(x).name);
                if (mActivities.get(x).name.contains("com.tapsformoney.activites")) {
                    if (!acName.contains(mActivities.get(x).name)) {
                        throw new RuntimeException("");

                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
        }
    }
}
